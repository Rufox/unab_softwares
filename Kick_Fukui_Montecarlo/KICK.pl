#!/usr/bin/perl -w

use strict;
use warnings;
# install: cpan Math::Matrix
use Math::Matrix;
# install: cpan Parallel::ForkManager
use Parallel::ForkManager;
# entrega cuando demora el proceso, cuanto de CPU utilizó, etc
use Benchmark;
use Data::Dump qw(dump ddx);
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# global variables
#
# # #
# check similarity
my $threshold_duplicate = 0.005;

# # #
# number of process
my $nprocess            = 80;

# # #
# Path for software mopac 
my $path_bin_mopac      = "MOPAC2016.exe";

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
my $num_atoms_xyz;

##############
# Hashs 
my $other_element = 0.8;
my %Atomic_radii = ( 'H'  => '0.4', 'He' => '0.3', 'Li' => '1.3', 'Be' => '0.9', 
                     'B'  => '0.8', 'C'  => '0.8', 'N'  => '0.8', 'O'  => '0.7', 
					 'F'  => '0.7', 'Ne' => '0.7', 'Na' => '1.5', 'Mg' => '1.3', 
					 'Al' => '1.2', 'Si' => '1.1', 'P'  => '1.1', 'S'  => '1.0', 
					 'Cl' => '1.0', 'Ar' => '1.0', 'K'  => '2.0', 'Ca' => '1.7', 
					 'Sc' => '1.4', 'Ti' => '1.4', 'V'  => '1.3', 'Cr' => '1.3', 
					 'Mn' => '1.4', 'Fe' => '1.3', 'Co' => '1.3', 'Ni' => '1.2', 
					 'Cu' => '1.4', 'Zn' => '1.3', 'Ga' => '1.3', 'Ge' => '1.2', 
					 'As' => '1.2', 'Se' => '1.2', 'Br' => '1.1', 'Kr' => '1.1', 
					 'Rb' => '2.1', 'Sr' => '1.9', 'Y'  => '1.6', 'Zr' => '1.5', 
					 'Nb' => '1.4', 'Au' => '1.4' );

my %Atomic_number = ( '89'  => 'Ac', '13'  => 'Al', '95'  => 'Am', '51'  => 'Sb',	
	                  '18'  => 'Ar', '33'  => 'As', '85'  => 'At', '16'  => 'S',  
					  '56'  => 'Ba', '4'   => 'Be', '97'  => 'Bk', '83'  => 'Bi',	
                      '107' => 'Bh', '5'   => 'B', 	'35'  => 'Br', '48'  => 'Cd',	
	                  '20'  => 'Ca', '98'  => 'Cf',	'6'   => 'C',  '58'  => 'Ce',	
	                  '55'  => 'Cs', '17'  => 'Cl',	'27'  => 'Co', '29'  => 'Cu',	
	                  '24'  => 'Cr', '96'  => 'Cm', '110' => 'Ds', '66'  => 'Dy',
	                  '105' => 'Db', '99'  => 'Es', '68'  => 'Er', '21'  => 'Sc',	
	                  '50'  => 'Sn', '38'  => 'Sr', '63'  => 'Eu', '100' => 'Fm',	
	                  '9'   => 'F',  '15'  => 'P',  '87'  => 'Fr', '64'  => 'Gd',	
	                  '31'  => 'Ga', '32'  => 'Ge', '72'  => 'Hf', '108' => 'Hs',	
                      '2'   => 'He', '1'   => 'H',  '26'  => 'Fe', '67'  => 'Ho',	
					  '49'  => 'In', '53'  => 'I',  '77'  => 'Ir', '70'  => 'Yb',
					  '39'  => 'Y',  '36'  => 'Kr', '57'  => 'La', '103' => 'Lr',	
					  '3'   => 'Li', '71'  => 'Lu', '12'  => 'Mg', '25'  => 'Mn',	
                      '109' => 'Mt', '101' => 'Md', '80'  => 'Hg', '42'  => 'Mo',	
					  '60'  => 'Nd', '10'  => 'Ne', '93'  => 'Np', '41'  => 'Nb',	
					  '28'  => 'Ni', '7'   => 'N',  '102' => 'No', '79'  => 'Au',	
					  '76'  => 'Os', '8'   => 'O', 	'46'  => 'Pd', '47'  => 'Ag',	
					  '78'  => 'Pt', '82'  => 'Pb',	'94'  => 'Pu', '84'  => 'Po',	
					  '19'  => 'K',  '59'  => 'Pr', '61'  => 'Pm', '91'  => 'Pa',	
					  '88'  => 'Ra', '86'  => 'Rn', '75'  => 'Re', '45'  => 'Rh',	
					  '37'  => 'Rb', '44'  => 'Ru', '104' => 'Rf', '62'  => 'Sm',
					  '106' => 'Sg', '34'  => 'Se', '14'  => 'Si', '11'  => 'Na',
					  '81'  => 'Tl', '73'  => 'Ta', '43'  => 'Tc', '52'  => 'Te',	
					  '65'  => 'Tb', '22'  => 'Ti', '90'  => 'Th', '69'  => 'Tm',	
					  '112' => 'Uub','116' => 'Uuh','111' => 'Uuu','118' => 'Uuo',	
					  '115' => 'Uup','114' => 'Uuq','117' => 'Uus','113' => 'Uut',
					  '92'  => 'U',  '23'  => 'V',  '74'  => 'W',  '54'  => 'Xe',
                      '30'  => 'Zn', '40'  => 'Zr' );

###################################
# Verification
sub verification{
	my ($a1, $a2, $dist)=@_;
	# hash values	
	my $v1  = $Atomic_radii{$a1} || $other_element; 
	my $v2  = $Atomic_radii{$a2} || $other_element;
	my $sum = $v1 + $v2;  
	my $resultado;
	# steric effects if radio1+radio2 < distance
	if($dist <= $sum){
		# Steric problem	
		$resultado = 1; 
	}else{
		$resultado = 0;
	}
	return $resultado;
}
###################################
# Steric Impediment
sub steric_impediment {
	# array are send by reference
	my ($frag_1,$frag_2) = @_;
	#
	# reference arrays	
	#	my @coords1 = @{$frag_1}; 
	#	my @coords2 = @{$frag_2};
	# get size
	my $final_trial = 0;
	my $resultado   = 0;
	#
	foreach my $key (sort(keys %$frag_2)) {
		my @coords1 = @{%$frag_1{$key}};
		my @coords2 = @{%$frag_2{$key}};
		# 
		for (my $i=0; $i < scalar(@coords1);$i++){
			for (my $j=0; $j < scalar(@coords2); $j++){
				my ($atom_1,$axis_x_1,$axis_y_1,$axis_z_1) = split '\s+', $coords1[$i];
				my ($atom_2,$axis_x_2,$axis_y_2,$axis_z_2) = split '\s+', $coords2[$j];
				my $distance    = Euclidean_distance($axis_x_1,$axis_y_1,$axis_z_1,$axis_x_2,$axis_y_2,$axis_z_2);
				$final_trial = $final_trial + verification($atom_1,$atom_2,$distance);
				if( $final_trial ==	 1 ) {
					$resultado = 1;
					last;
				}
			}
		}
	}
	# verify for steric impediment, 1 yes, 0 no;
	return $resultado;				
}
###################################
# input file gaussian 
sub G03Input {
	#
	my $filebase     = $_[0];
	my $G03Input     = "$filebase.com";
	my $Header       = $_[1];
	my $ncpus        = $_[2];
	my $mem          = $_[3];
	my $Charge       = $_[4];
	my $Multiplicity = $_[5];
	my $coordsMM     = $_[6];
	my $iteration    = $_[7];
	#
	open (COMFILE, ">$G03Input");
	print COMFILE "%chk=$filebase.chk\n";
	if ( $ncpus > 0 ) {
		print COMFILE "%NProc=$ncpus\n";
	}	
	(my $word_nospaces = $mem) =~ s/\s//g;
	print COMFILE "%mem=$word_nospaces"."GB\n";
	print COMFILE "# $Header \n";
	print COMFILE "\nKick job $iteration\n";
	print COMFILE "\n";
	print COMFILE "$Charge $Multiplicity\n";
	print COMFILE "$coordsMM\n";
	print COMFILE "\n";
	close COMFILE;
}
###################################
# input file Mopac
sub MopacInput {
	#
	my $filebase     = $_[0];
	my $coordsMM     = $_[1];
	my $MopacInput   = "$filebase.mop";
	my $iteration    = $_[2];
	my $Headerfile   = $_[3];
	my $Charge       = $_[4];
	my $Multiplicity = $_[5];
	#
	my $mem          = $_[7];
	#
	my $tmp   = 1;
	my @words = split (/\n/,$coordsMM);
	#
	open (COMFILE, ">$MopacInput");
	#
	my $word;
	# Spin multiplicity:
	if ( $Multiplicity == 0 ) { $word = "NONET"   };			
	# singlet	- 0 unpaired electrons
	if ( $Multiplicity == 1 ) { $word = "SINGLET" };
	# doublet	- 1 unpaired electrons
	if ( $Multiplicity == 2 ) { $word = "DOUBLET" };
	# triplet	- 2 unpaired electrons
	if ( $Multiplicity == 3 ) { $word = "TRIPLET" };
	# quartet	- 3 unpaired electrons
	if ( $Multiplicity == 4 ) { $word = "QUARTET" };
	# quintet	- 4 unpaired electrons			
	if ( $Multiplicity == 5 ) { $word = "QUINTET" };
	# sextet	- 5 unpaired electrons
	if ( $Multiplicity == 6 ) { $word = "SEXTET"  };
	# septet	- 6 unpaired electrons
	if ( $Multiplicity == 7 ) { $word = "SEPTET"  };
	# octet	- 7 unpaired electrons
	if ( $Multiplicity == 8 ) { $word = "OCTET"   };
	#
	my $ncpus        = ($_[6] * 2);
	if ( $ncpus == 0 ) {
		print COMFILE "$Headerfile $word CHARGE=$Charge";
	} else {
		# The maximum number of threads is normally equal to the number of cores, 
		# even if each core supports two threads.
		# In the special case of THREADS=1, parallelization is switched off.
		print COMFILE "$Headerfile $word CHARGE=$Charge THREADS=$ncpus";
	}	
	print COMFILE "\n";
	print COMFILE "Kick job $iteration\n";
	print COMFILE "\n";	
	foreach my $i (@words){
		my @axis    = split (" ",$i);
		#
		my $label  = $axis[0];  
		my $axis_x = $axis[1];
		my $axis_y = $axis[2];
		my $axis_z = $axis[3];
		#
		print COMFILE "$label\t$axis_x\t$tmp\t$axis_y\t$tmp\t$axis_z\t$tmp\n";
	}
	print COMFILE "\n";
	print COMFILE "\n";
	close (COMFILE);
	#
	return $MopacInput;
}
###################################
# Generation of random co-ordinates
sub gen_xyz {
	my ($Box_x, $Box_y, $Box_z) = @_;
	# generate a random number in perl in the range box size
	my $lower_limit_x = ($Box_x * -1);
	my $upper_limit_x = $Box_x;
	my $lower_limit_y = ($Box_y * -1);
	my $upper_limit_y = $Box_y;
	my $lower_limit_z = ($Box_z * -1);
	my $upper_limit_z = $Box_z;
	#
	my $x = (rand($upper_limit_x-$lower_limit_x) + $lower_limit_x);
	my $y = (rand($upper_limit_y-$lower_limit_y) + $lower_limit_y);
	my $z = (rand($upper_limit_z-$lower_limit_z) + $lower_limit_z);
	#
	my $x_coord = sprintf '%.6f', $x;
	my $y_coord = sprintf '%.6f', $y;
	my $z_coord = sprintf '%.6f', $z;
	my @coords  = ($x_coord, $y_coord, $z_coord);
	return @coords;
}
###################################
# phi, theta, psi
sub gen_ptp {
	my $pi     = 3.14159265;
	my $phi    = sprintf '%.6f', rand()*2*$pi;
	my $theta  = sprintf '%.6f', rand()*2*$pi;
	my $psi    = sprintf '%.6f', rand()*2*$pi;
	my @angles = ($phi, $theta, $psi);
	return @angles;
}
###################################
# compute the center of mass
sub measure_center {
	my ($coord_x,$coord_y,$coord_z) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array  = ();
	my $weight = 1;
	# variable sum
	my $sum_weight = 0;
	my $sum_x = 0;
	my $sum_y = 0;
	my $sum_z = 0;
	for ( my $j = 0 ; $j < $num_data ; $j = $j + 1 ){
		$sum_weight+= $weight;
		$sum_x+= $weight * @$coord_x[$j];
		$sum_y+= $weight * @$coord_y[$j];
		$sum_z+= $weight * @$coord_z[$j];		
	}
	my $com_x = $sum_x / $sum_weight;
	my $com_y = $sum_y / $sum_weight;
	my $com_z = $sum_z / $sum_weight;
	# array
	@array = ($com_x,$com_y,$com_z);
	# return array	
	return @array;
}
###################################
# Returns the additive inverse of v(-v)
sub vecinvert {
	my ($center_mass) = @_;
	my @array         = ();
	foreach my $i (@$center_mass) {
		my $invert        = $i * -1;
		$array[++$#array] = $invert; 
	}	
	# return array	
	return @array;
}
###################################
# Returns the vector sum of all the terms.
sub vecadd {
	my ($coord_x,$coord_y,$coord_z,$vecinvert_cm ) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array   = ();
	my $sum_coord_x;
	my $sum_coord_y;
	my $sum_coord_z;
	# array 
	my @array_x = ();
	my @array_y = ();
	my @array_z = ();
	for ( my $i = 0 ; $i < $num_data ; $i = $i + 1 ){	
		$sum_coord_x = @$coord_x[$i]+@$vecinvert_cm[0] ; 
		$sum_coord_y = @$coord_y[$i]+@$vecinvert_cm[1] ;
		$sum_coord_z = @$coord_z[$i]+@$vecinvert_cm[2] ;
		# save array
		$array_x[++$#array_x] = $sum_coord_x;
		$array_y[++$#array_y] = $sum_coord_y;
		$array_z[++$#array_z] = $sum_coord_z;
	}
	@array = ( [@array_x], 
              [@array_y], 
              [@array_z] ); 
	# return array	
	return @array;
}
###################################
# drawing a box around a molecule 
sub box_molecule {
	my ($coordsmin, $coordsmax) = @_;
	#
	my $minx = @$coordsmin[0];
	my $maxx = @$coordsmax[0];
	my $miny = @$coordsmin[1];
	my $maxy = @$coordsmax[1];
	my $minz = @$coordsmin[2];
	my $maxz = @$coordsmax[2];
	# raw the lines
	
	my $filename = 'BOX_kick.vmd';
	open(my $fh, '>', $filename) or die "Could not open file '$filename' $!";	
	print $fh "draw delete all\n";
	print $fh "draw materials off\n";
	print $fh "draw color green\n";
	#
	print $fh "draw line \"$minx $miny $minz\" \"$maxx $miny $minz\" \n";
	print $fh "draw line \"$minx $miny $minz\" \"$minx $maxy $minz\" \n";
	print $fh "draw line \"$minx $miny $minz\" \"$minx $miny $maxz\" \n";
	#
	print $fh "draw line \"$maxx $miny $minz\" \"$maxx $maxy $minz\" \n";
	print $fh "draw line \"$maxx $miny $minz\" \"$maxx $miny $maxz\" \n";
	#
	print $fh "draw line \"$minx $maxy $minz\" \"$maxx $maxy $minz\" \n";
	print $fh "draw line \"$minx $maxy $minz\" \"$minx $maxy $maxz\" \n";
	#
	print $fh "draw line \"$minx $miny $maxz\" \"$maxx $miny $maxz\" \n";
	print $fh "draw line \"$minx $miny $maxz\" \"$minx $maxy $maxz\" \n";
	#
	print $fh "draw line \"$maxx $maxy $maxz\" \"$maxx $maxy $minz\" \n";
	print $fh "draw line \"$maxx $maxy $maxz\" \"$minx $maxy $maxz\" \n";
	print $fh "draw line \"$maxx $maxy $maxz\" \"$maxx $miny $maxz\" \n";
	close $fh;
}					   
###################################
# Automatic box size
sub automatic_box_size {
	my ($input_array) = @_;
	my $sum = 0;
	#
	foreach my $i (@{$input_array}) {
		my $radii_val;
		if ( exists $Atomic_radii{$i} ) {
			# exists
			$radii_val = $Atomic_radii{$i};
		} else {
			# not exists
			$radii_val = $other_element ;
		}
		$sum+=$radii_val;
	}
	return $sum;
}
###################################
# Keywords Errors
sub errors_config {
	my ($data) = @_;
	my $bolean = 1;
	if ( ( @$data[0]  =~/kick_numb_input/gi )  ){ } else { print "ERROR Correct Keywords: kick_numb_input\n";  $bolean = 0;};
	if ( ( @$data[1]  =~/box_size/gi )         ){ } else { print "ERROR Correct Keywords: box_size\n";         $bolean = 0;};
	if ( ( @$data[2]  =~/fragments/gi )        ){ } else { print "ERROR Correct Keywords: fragments\n";        $bolean = 0;};
	if ( ( @$data[3]  =~/core_mem/gi )         ){ } else { print "ERROR Correct Keywords: core_mem\n";         $bolean = 0;};
	if ( ( @$data[4]  =~/charge_multi/gi)      ){ } else { print "ERROR Correct Keywords: charge_multi\n";     $bolean = 0;};
	if ( ( @$data[5]  =~/header/gi)            ){ } else { print "ERROR Correct Keywords: header\n";           $bolean = 0;};
	if ( ( @$data[6]  =~/fragm_fix/gi)         ){ } else { print "ERROR Correct Keywords: fragm_fix\n";        $bolean = 0;};	
	if ( ( @$data[7]  =~/software/gi)          ){ } else { print "ERROR Correct Keywords: software\n";         $bolean = 0;};
	return $bolean;
}
###################################
# read files
sub read_file {
	# filename
	my ($filename) = @_;
	(my $input_file = $filename) =~ s/\s//g;
	my @array          = ();
	# open file
	open(FILE, "<", $input_file ) || die "Can't open $input_file: $!";
	while (my $row = <FILE>) {
		chomp($row);
		push (@array,$row);
	}
	close (FILE);
	# return array	
	return @array;
}
###################################
# join string
sub string_tmp {
	my ($array_input) = @_;
	#
	my $concat_string;
	for ( my $i=2 ; $i < scalar (@{$array_input}); $i++) {
		$concat_string.="@$array_input[$i] ";
	}
	return $concat_string;
}
###################################
# function to determine if a string is numeric
sub looks_like_number {
	my ($array_input) = @_;	
	#
	my $number;
	my $element;
	my @array_elements = ();
	for ( my $i=2 ; $i < scalar (@{$array_input}); $i++) {
		$number  = 0;
		if ( @$array_input[$i] =~ /^[0-9,.E]+$/ ) {
			$number = @$array_input[$i];
		} else {
			$element = @$array_input[$i];
		}
		for ( my $j = 0 ; $j < $number ; $j++) {
			push (@array_elements,$element);
		}
	}
	return @array_elements;
}
###################################
# min selection: Returns one vectors containing the minimum x,y and z coordinates
sub min_vector {
	my ($coord_x,$coord_y,$coord_z) = @_;
	my @array = ();
	# variables
	my $minx  = 0; 
	my $miny  = 0;
	my $minz  = 0;
	# sorted coords array
	my @sorted_coord_x = sort { $a <=> $b } @$coord_x ;
	my @sorted_coord_y = sort { $a <=> $b } @$coord_y ;
	my @sorted_coord_z = sort { $a <=> $b } @$coord_z ;
	# assign min coord
	$minx  = $sorted_coord_x[0] ;
	$miny  = $sorted_coord_y[0] ;
	$minz  = $sorted_coord_z[0] ;
	@array = ($minx, $miny, $minz);
	# return array	
	return @array;
}
###################################
# max selection: Returns one vectors containing the minimum x,y and z coordinates
sub max_vector {
	my ($coord_x,$coord_y,$coord_z) = @_;
	my @array = ();
	# variables
	my $maxx  = 0; 
	my $maxy  = 0;
	my $maxz  = 0;
	# sorted coords array
	my @sorted_coord_x = sort { $b <=> $a } @$coord_x ;
	my @sorted_coord_y = sort { $b <=> $a } @$coord_y ;
	my @sorted_coord_z = sort { $b <=> $a } @$coord_z ;
	# assign min coord
	$maxx  = $sorted_coord_x[0] ;
	$maxy  = $sorted_coord_y[0] ;
	$maxz  = $sorted_coord_z[0] ;
	@array = ($maxx, $maxy, $maxz);
	# return array		
	return @array;
}
###################################
# logo
sub logo {
	print "                 ____  __.__        __    \n";
	print "                |    |/ _|__| ____ |  | __ \n";
	print "                |      < |  |/ ___\\|  |/ / \n";
	print "                |    |  \\|  \\  \\___|    <  \n";
	print "                |____|__ \\__|\\___  >__|_ \\ \n";
	print "                        \\/       \\/     \\/ \n";
	print "\n                     TiznadoLab\n";
	print "\n";
	my $datestring = localtime();
	print "              $datestring\n\n";
}
###################################
# Euclidean distance between points
sub Euclidean_distance {
	# array coords basin 1 and basin 2
	my ($p1,$p2,$p3, $axis_x, $axis_y, $axis_z) = @_;
	# variables
	my $x1 = $axis_x;
	my $y1 = $axis_y;
	my $z1 = $axis_z;
	# measure distance between two point
	my $dist = sqrt(
					($x1-$p1)**2 +
					($y1-$p2)**2 +
					($z1-$p3)**2
					); 
	return $dist;
}
###################################
# Between points
sub Mult_coords {
	# array coords basin 1 and basin 2
	my ($p1,$p2,$p3, $axis_x, $axis_y, $axis_z) = @_;
	# variables
	my $x1 = $axis_x;
	my $y1 = $axis_y;
	my $z1 = $axis_z;
	# measure distance between two point
	my $dist = 	$x1*$p1 +
				$y1*$p2 +
				$z1*$p3 ; 
	return $dist;
}
###################################
# promedio
sub promedio {
	my ($num,$data) = @_;
	# write file
	my $sum = 0;
	for ( my $i = 0 ; $i < $num ; $i = $i + 1 ){
		$sum+= @$data[$i];
	}
	my $div = $sum / $num;
	return $div; 
}
###################################
# Grigoryan Springborg similitud
sub Grigoryan_Springborg {
	my ($numb_atoms,$array_coord_x_1,$array_coord_y_1, $array_coord_z_1,
	                $array_coord_x_2,$array_coord_y_2, $array_coord_z_2) = @_;
	#
	my @distance_alpha = ();
	my @distance_beta  = ();
	#
	my $sum_1 = 0;
	for ( my $i = 0 ; $i < $numb_atoms ; $i = $i + 1 ){
		for ( my $j = 0 ; $j < $numb_atoms ; $j = $j + 1 ){
			if ( $i < $j ){
				my $distance = Euclidean_distance (@$array_coord_x_1[$i],@$array_coord_y_1[$i],@$array_coord_z_1[$i],
												@$array_coord_x_1[$j],@$array_coord_y_1[$j],@$array_coord_z_1[$j]);
				push (@distance_alpha,$distance);
			}
		}
	}
	#
	my $sum_2 = 0;
	for ( my $i = 0 ; $i < $numb_atoms ; $i = $i + 1 ){
		for ( my $j = 0 ; $j < $numb_atoms ; $j = $j + 1 ){
			if ( $i < $j ){
				my $distance = Euclidean_distance (@$array_coord_x_2[$i],@$array_coord_y_2[$i],@$array_coord_z_2[$i],
												@$array_coord_x_2[$j],@$array_coord_y_2[$j],@$array_coord_z_2[$j]);
				push (@distance_beta,$distance);
			}
		}
	}
	#
	my $InterDist_1 = (2/($numb_atoms*($numb_atoms-1)));
	my $InterDist_2 = (($numb_atoms*($numb_atoms-1))/2);  
	#
	my @mol_alpha = ();
	my @mol_beta  = ();
	my @idx_1 = sort { $distance_alpha[$a] <=> $distance_alpha[$b] } 0 .. $#distance_alpha;
	my @idx_2 = sort { $distance_beta[$a]  <=> $distance_beta[$b]  } 0 .. $#distance_beta;
	@mol_alpha = @distance_alpha[@idx_1];
	@mol_beta  = @distance_beta[@idx_2];
	#
	my $num_1 = scalar (@mol_alpha);
	my $num_2 = scalar (@mol_beta);
	my $dim_alpha =  promedio ($num_1,\@mol_alpha);
	my $dim_beta  =  promedio ($num_2,\@mol_beta);
	#
	my $sumX;
	my $sumY;
	# Sin normalizar
	for ( my $i = 0 ; $i < $InterDist_2 ; $i = $i + 1 ){
		my $mult = ( $mol_alpha[$i] - $mol_beta[$i] )**2;
		$sumX+=$mult;
	}
	my $Springborg_1 = sqrt( $InterDist_1 * $sumX );
	# Normalizado
	for ( my $i = 0 ; $i < $InterDist_2 ; $i = $i + 1 ){
		my $mult = ( ($mol_alpha[$i]/$dim_alpha) - ($mol_beta[$i]/$dim_beta) )**2;
		$sumY+=$mult;
	}
	my $Springborg_2 = sqrt( $InterDist_1 * $sumY );
	#
	return $Springborg_2; 
}
###################################
# delete repeat data
sub uniq {
	my %seen;
	grep !$seen{$_}++, @_;
}
###################################
# index duplicate data
sub index_elements {
	my ($duplicate_name,$files_name) = @_;
	# reference arrays	
	my @array_1     = @{$duplicate_name}; 
	my @array_2     = @{$files_name};
	my @array_index = ();
	#
	my @filtered = uniq(@array_1);
	foreach my $u (@filtered){
		my @del_indexes = reverse( grep { $array_2[$_] eq "$u" } 0..$#array_2);
		foreach my $k (@del_indexes) {
			push (@array_index,$k);
		}
	}
	return @array_index;
}
###################################
# verify similar structure
sub info_duplicate_structures {
	my ($number_cycle,$numb_atoms,$coords_xyz,$ncpus) = @_;
	my @array_coords = @{$coords_xyz};
	#	
#	$threshold_duplicate = 0.;
	#
	my $sum                 =  ($number_cycle + ($number_cycle/2));
	my $add                 =  int ($number_cycle);
	#
	my %Info_Coords = ();
	my @array_keys  = ();
	for (my $i=0; $i < $add ; $i++) { 
		my $id            = sprintf("%06d",$i);
		my @abc           = split (/\n/,$array_coords[$i]);
		$Info_Coords{$id} = \@abc;
		push(@array_keys,$id);
	}
	my $pm        = new Parallel::ForkManager($ncpus);
	my $iteration = 0;
	#
	my $file_tmp = "Dupli.tmp";
	open (FILE, ">$file_tmp") or die "Unable to open XYZ file: $file_tmp";
	my $file_log = "Duplicates_info.log";
	open (LOGDUPLI, ">$file_log") or die "Unable to open XYZ file: $file_log"; 
	print LOGDUPLI "\n# # # SUMMARY SIMILAR STRUCTURES # # #\n\n";
	for ( my $x = 0 ; $x < scalar (@array_keys); $x = $x + 1 ) {
		$pm->start($iteration) and next;
		# All children process havee their own random.			
		srand();
		for ( my $y = 0 ; $y < scalar (@array_keys); $y = $y + 1 ) {
			if ( $x < $y ){
				#
				my @matrix_1 = @{$Info_Coords{$array_keys[$x]}};
				my @matrix_2 = @{$Info_Coords{$array_keys[$y]}};
				# # # # # # # # # # # # # # # # #
				#
				my @array_name_atoms_1 = ();
				my @array_coord_x_1    = ();
				my @array_coord_y_1    = ();
				my @array_coord_z_1    = ();
				#
				my @array_name_atoms_2 = ();
				my @array_coord_x_2    = ();
				my @array_coord_y_2    = ();
				my @array_coord_z_2    = ();	
				#
				for ( my $i = 0 ; $i < $numb_atoms ; $i = $i + 1 ){
					my @array_tabs_1  = split (/\s+/,$matrix_1[$i]);
					#
					my $radii_val;
					my $other_element = 0;
					if ( exists $Atomic_number{$array_tabs_1[0]} ) {
						# exists
						$radii_val = $Atomic_number{$array_tabs_1[0]};
						$array_name_atoms_1[++$#array_name_atoms_1] = $radii_val;
					} else {
						# not exists
						$radii_val = $array_tabs_1[0] ;
						$array_name_atoms_1[++$#array_name_atoms_1] = $radii_val;
					}

					$array_coord_x_1[++$#array_coord_x_1]   = $array_tabs_1[1];
					$array_coord_y_1[++$#array_coord_y_1]   = $array_tabs_1[2];
					$array_coord_z_1[++$#array_coord_z_1]   = $array_tabs_1[3];
				}
				#
				for ( my $i = 0 ; $i < $numb_atoms ; $i = $i + 1 ){
					my @array_tabs_2 = split (/\s+/,$matrix_2[$i]);
					#
					my $radii_val;
					my $other_element = 0;
					if ( exists $Atomic_number{$array_tabs_2[0]} ) {
						# exists
						$radii_val = $Atomic_number{$array_tabs_2[0]};
						$array_name_atoms_2[++$#array_name_atoms_2] = $radii_val;
					} else {
						# not exists
						$radii_val = $array_tabs_2[0] ;
						$array_name_atoms_2[++$#array_name_atoms_2] = $radii_val;
					}
					$array_coord_x_2[++$#array_coord_x_2]   = $array_tabs_2[1];
					$array_coord_y_2[++$#array_coord_y_2]   = $array_tabs_2[2];
					$array_coord_z_2[++$#array_coord_z_2]   = $array_tabs_2[3];
				}
				my $Springborg = Grigoryan_Springborg ($numb_atoms,\@array_coord_x_1 ,\@array_coord_y_1 ,\@array_coord_z_1 
																,\@array_coord_x_2 ,\@array_coord_y_2 ,\@array_coord_z_2 );												  
				#
				if ( $Springborg < $threshold_duplicate ) {
					my $number      = sprintf '%.6f', $Springborg;
					print FILE "$array_keys[$y]\n";
					print FILE "Value = $number\n";
					print LOGDUPLI "# Isomer$array_keys[$x] ~= Isomer$array_keys[$y]\n";
					print LOGDUPLI "# Value = $number\n";
					print LOGDUPLI "------------------------\n";
				}
				#
			}
			$iteration++;
		}
		$pm->finish;	
	}
	close (FILE);
	# Paralel
	$pm->wait_all_children;
	# # #
	my @data_tmp = read_file ($file_tmp);	
	my @duplicates_name = ();
	my @Value_simi      = ();
	foreach my $info (@data_tmp) {
		if ( ($info =~ m/Value/) ) {
			my @array_tabs = ();
			@array_tabs    = split ('\s+',$info);
			push (@Value_simi,$array_tabs[2]);
		} else {
			push (@duplicates_name,$info);
		}
	}
	# Delete similar structures
	my @index_files = index_elements (\@duplicates_name,\@array_keys);
	my $file_xyz = "Duplicates_coords.xyz";
	open (DUPLIXYZ, ">$file_xyz") or die "Unable to open XYZ file: $file_log"; 
	my $count_sim_struc = 0;
	foreach my $id_index (@index_files) {
		my @coords_dup = @{$Info_Coords{$array_keys[$id_index]}};
		print DUPLIXYZ scalar (@coords_dup);
		print DUPLIXYZ "\n";
		print DUPLIXYZ "Isomer$array_keys[$id_index] Duplicate\n";
		foreach (@coords_dup) {
			print DUPLIXYZ "$_\n";
		}
		$count_sim_struc++;
	}
	print LOGDUPLI "\nNumber of Similar Structures = $count_sim_struc\n";
	close (LOGDUPLI);
	close (DUPLIXYZ);
	#
	# Delete similar structures	
	for my $k (@index_files) {
		delete $Info_Coords{$array_keys[$k]};
	}
	#
	unlink ($file_tmp);
	#
	return \%Info_Coords;
}
###################################
# Energy ouputsmopac
sub energy_mopac {
	my ($num_atoms_xyz, $name_file) = @_;	
	# directorio
	my $dir = './';
	#
	my @array = ();
	my @array_coords_mopac = ();
	# abrir directorio
	opendir(DIR, $dir) or die $!;
	#
	while (my $file = readdir(DIR)) {
		# Use a regular expression to ignore files beginning with a period
		next if ($file =~ m/^\./);
		next unless ($file =~ m/\.arc$/);
		# sin extension
		my $fileSnExt = $file;
		$fileSnExt =~ s/\..*$//;
		# 
		push (@array,$file);
		#
	}
	closedir(DIR);
	#
	my $tam_esc = scalar (@array);
	if ($tam_esc == 0) { print "ERROR problem MOPAC $tam_esc files .arc, check .out\n"; exit(0);}
	#
	my @HeaderLines = ();
	my @ZeroPoint   = ();
	my @energyy     = ();
	my $energy      = '';
	my $number_atoms = $num_atoms_xyz;
	foreach my $i (@array) {
		#
		open(HEADER,"$i") or die "Unable to open $i";
		@HeaderLines  = <HEADER>;
		close HEADER;
		#
		while (my $HLine = shift (@HeaderLines)) {
			chomp ($HLine);
			#
			my $hatfield_1 = "TOTAL ENERGY";
			if ( $HLine =~/$hatfield_1/ ){
				$energy = $HLine;
				my @words_1 = split (" ",$energy);
				push (@energyy,$words_1[3]); 
			}
		}
	}
	#
	my @idx = sort { $energyy[$a] <=> $energyy[$b] } 0 .. $#energyy;
	my @energyy_1     = @energyy[@idx];
	my @array_1       = @array[@idx];
	#
	my $mopac_all_geome = $name_file; 
	open (ARCFILE, ">$mopac_all_geome");
	#
	for ( my $i = 0; $i < scalar(@array_1); $i++) {
		# Convert kcal/mol
		my $kcal          = 23.0605419453;
		my $theBest_E     = abs($energyy_1[0] - $energyy_1[$i]);
		# Total Energy
		my $convert_E     = ($theBest_E * $kcal);	
		#
		open (HEADER,"$array_1[$i]") or die "Unable to open $i";
		my @HeaderLines = <HEADER>;
		close HEADER;
		#
		my $count_lines = 0;
		my $first_line  = 0;
		my @array_lines = ();
		while (my $HLine = shift (@HeaderLines)) {
			my $hatfield = "FINAL GEOMETRY OBTAINED";
			if ( $HLine =~/$hatfield/ ){
				$first_line = $count_lines;			
			}
			$count_lines++;
			push (@array_lines,$HLine);
		}
		my $tmp_rest = $first_line + 3;	
		my $lala     =  $count_lines;
		#
		my $concat;
		#
		print ARCFILE "$number_atoms\n";
		print ARCFILE "$array_1[$i] ";
		print ARCFILE "$energyy_1[$i] eV ";
		my $number = sprintf '%05f', $convert_E;
		print ARCFILE "$number Kcal/mol\n";
		for ( my $i = $tmp_rest; $i < $count_lines; $i++) {
			my $wordlength = length ($array_lines[$i]);    
			#
			if ( $wordlength > 3) {
				chomp ($array_lines[$i]);
				my @words = split (" ",$array_lines[$i]);
				print ARCFILE "$words[0]\t$words[1]\t$words[3]\t$words[5]\n";
				$concat.= "$words[0]\t$words[1]\t$words[3]\t$words[5]\n";
			}
		}
		push (@array_coords_mopac,$concat);
	}
	close ARCFILE;
	return @array_coords_mopac;
}
###################################
# Check inside the box
sub check_inside_box {
	# array are send by reference
	my ($frag,$side_x,$side_y,$side_z) = @_;
	# get size
	my $resultado   = 0;
	my $nprocess    = 40;
	# 
	foreach my $key (sort(keys %$frag)) {
		my @coords = @{%$frag{$key}};
		# 
		for (my $i=0; $i < scalar(@coords);$i++){
			my ($atom,$axis_x,$axis_y,$axis_z) = split '\s+', $coords[$i];
			my $option_x = box_space ($side_x,$axis_x);
			my $option_y = box_space ($side_y,$axis_y);
			my $option_z = box_space ($side_z,$axis_z);
			#
			my $final_trial = $option_x + $option_z + $option_y;
			#
			if( $final_trial ==	 1 ) {
				$resultado = 1;
				last;
			}
		}
	}
	# verify for steric impediment, 1 yes, 0 no;
	return $resultado;		
}
###################################
# Check inside the box min max
sub check_inside_box_min_max {
	# array are send by reference
	my ($frag,$side_x,$side_y,$side_z) = @_;
	# get size
	my $resultado   = 0;
	# 
	my @axis_x = ();
	my @axis_y = ();
	my @axis_z = ();
	foreach my $key (sort(keys %$frag)) {
		my @coords = @{%$frag{$key}};
		for (my $i=0; $i < scalar(@coords);$i++){
			my ($atom,$axis_x,$axis_y,$axis_z) = split '\s+', $coords[$i];
			push (@axis_x,$axis_x);
			push (@axis_y,$axis_y);
			push (@axis_z,$axis_z);
		}
	}
	my ($min_x,$min_y,$min_z) = min_vector (\@axis_x,\@axis_y,\@axis_z);
	my ($max_x,$max_y,$max_z) = max_vector (\@axis_x,\@axis_y,\@axis_z);
	#
	my $option_min_x = box_space ($side_x,$min_x);
	my $option_min_y = box_space ($side_y,$min_y);
	my $option_min_z = box_space ($side_z,$min_z);
	#
	my $option_max_x = box_space ($side_x,$max_x);
	my $option_max_y = box_space ($side_y,$max_y);
	my $option_max_z = box_space ($side_z,$max_z);
	#
	my $final_trial = $option_min_x + $option_min_y + $option_min_z +
	                  $option_max_x + $option_max_y + $option_max_z ;
	if( $final_trial ==	 1 ) {
		$resultado = 1;
	}
	# verify for steric impediment, 1 yes, 0 no;
	return $resultado;		
}
###################################
# Box space search 
sub box_space {
	# array are send by reference
	my ($side,$coord) = @_;
	my $limit_pos = ( +1 * ( abs ($side) + 1));
	my $limit_neg = ( -1 * ( abs ($side) + 1));
	#
	my $option;
	if ( ( $coord < $limit_pos ) && ( $coord > $limit_neg ) ) {
		$option = 0;
	} else {
		$option = 1;
	}
	return $option;
}

#####################################
#DIEGO CODE HERE
sub CIntegralCalculator{
	my ($frag_1) = @_;
	#
	# reference arrays	
	#	my @coords1 = @{$frag_1}; 
	#	my @coords2 = @{$frag_2};
	# get size
	#
	my $max= keys %$frag_1;
	#Como ya se verfico el impedimeto esterico, aqui solo se vera la ecuacion
	my @Fukui=();
	my $superSum=0;
	for (my $i = 0; $i < $max; $i++) {
		for (my $j = $i+1; $j < $max; $j++) {
			my @Coord1=@{%$frag_1{$i}};
			my @Coord2=@{%$frag_1{$j}};
			my $sum=0;
			for (my $h = 0; $h < scalar @Coord1; $h++) {
				for (my $k = 0; $k < scalar @Coord2; $k++) {
					#print "$Coord1[$k][3]\n";
					my $Multi=$Coord1[$h][3]*$Coord2[$k][3];
					my $dists=Euclidean_distance($Coord1[$h][0],$Coord1[$h][1],$Coord1[$h][2],$Coord2[$k][0],$Coord2[$k][1],$Coord2[$k][2]);
					my $division=$Multi/$dists;
					#print "Fukui= $division";
					$sum=$sum+$division;
				}
				
			}
			print "Fukui ($i con $j)= $sum\n";
			$superSum=$superSum+$sum;
			push @Fukui, $sum;
		}
	}
	print "SUMA FINAL: $superSum\n";
	push @Fukui, $superSum;
	return @Fukui;
	
	# verify for steric impediment, 1 yes, 0 no;
}
####################################
################################
# if less than two arguments supplied,
# display usage
my ($file_name) = @ARGV;
if (not defined $file_name) {
	die "\nStochastic Search Kicks must be run with:\n\nUsage:\n\t perl kick.pl [configure-file]\n";
	exit;  
}
logo ();

my $tiempo_inicial = new Benchmark; #funcion para el tiempo de ejecucion del programa

#
my @delete_rot;
my @arrayOutputs = ();
# read and parse files
my @data          = read_file($file_name);
my @arrays_errors = ();
# data parse
my $Num_of_geometries_input  = 0;
my $Num_of_geometries_output = 0;
#
my $Box;
my @Box_dimensions = ();
my ($Box_x,$Box_y,$Box_z );
my $option_box;
#
my $atoms;
my @Atoms        = ();
my $Num_of_atoms = 0;

my $fragments    = 0;
my @Fragments    = ();
my $Num_of_fragments; 
#
my $Submit_guff;
my @Submit_parameters = ();
my $ncpus;
my $mem;
#
my $charge_multi;
my @charge_multi_parameters = ();
my $Charge; 
my $Multiplicity;
#
my $header;
my $fragm_fix;
#
my $software;
#
my $init_relax;
my $soft_relax;
#
foreach my $a_1 (@data){
	if ( ($a_1=~/#/gi ) ){
	#	print "$a_1\n";
	} else {
		if ( ($a_1=~/kick_numb_input/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);		
			# Identify empty string
			if (!defined($tmp[2])) {			
				print "ERROR input population numbers empty\n";
				exit;
			} else {
				$Num_of_geometries_input = $tmp[2];
			}	
			#
			$arrays_errors[0] = "kick_numb_input";
		}		
		if ( ($a_1=~/box_size/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string			
			if (!defined($tmp[2])) {
				print "MESSAGE Automatic size box\n";
				$option_box = 0;
			} else {
				my $var_tmp = string_tmp (\@tmp);
				$Box = $var_tmp;
				@Box_dimensions = split(/,/, $Box);
				$Box_x = $Box_dimensions[0];
				$Box_y = $Box_dimensions[1];
				$Box_z = $Box_dimensions[2];
				$option_box = 1;
			}
			#
			$arrays_errors[1] = "box_size";			
		}
		if ( ($a_1=~/fragments/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string	
			if (!defined($tmp[2])) {
				print "WARNING fragments empty\n";
				$Num_of_fragments = 0;
			} else {
				my $var_tmp       = string_tmp (\@tmp);			
				$fragments        = $var_tmp;
				@Fragments        = split(/,/, $fragments);
				$Num_of_fragments = scalar (@Fragments);
			}
			#
			$arrays_errors[2] = "fragments";			
		}
		if ( ($a_1=~/core_mem/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string
			if (!defined($tmp[2])) {
				# default cpus 1 and memory 1GB
				$ncpus             = 1;
				$mem               = 1;
			} else {
				my $var_tmp       = string_tmp (\@tmp);
				$Submit_guff       = $var_tmp;
				@Submit_parameters = split(/,/, $Submit_guff);
				$ncpus             = $Submit_parameters[0];
				$mem               = $Submit_parameters[1];
			}
			#
			$arrays_errors[3] = "core_mem";			
		}
		if ( ($a_1=~/charge_multi/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string
			if (!defined($tmp[2])) {
				# default multiplicity 1 and charge 0
				$Charge       = 0; 
				$Multiplicity = 1;
			} else {
				my $var_tmp   = string_tmp (\@tmp);
				$charge_multi = $var_tmp;
				@charge_multi_parameters = split(/,/, $charge_multi);
				$Charge       = $charge_multi_parameters[0]; 
				$Multiplicity = $charge_multi_parameters[1];
			}
			#
			$arrays_errors[4] = "charge_multi";			
		}
		if ( ($a_1=~/header/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string
			if (!defined($tmp[2])) {
				print "ERROR theory level empty\n";
				exit;
			} else {
				my $var_tmp = string_tmp (\@tmp);			
				$header     = $var_tmp;
			}
			#
			$arrays_errors[5] = "header";			
		}
		if ( ($a_1=~/fragm_fix/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string
			if (!defined($tmp[2])) {
				print "ERROR fix molecule empty\n";
				exit;
			} else {			
				$fragm_fix = $tmp[2];
			}
			#
			$arrays_errors[6] = "fragm_fix";			
		}
		if ( ($a_1=~/software/gi ) ){
			my @tmp = ();
			@tmp    = split (/\s+/,$a_1);
			# Identify empty string
			if (!defined($tmp[2])) {
				print "ERROR software empty\n";
				exit;
			} else {			
				$software = $tmp[2];
			}
			#
			$arrays_errors[7] = "software";			
		}

	}
}
#
if (!defined($arrays_errors[0]))  { $arrays_errors[0]  = "NO"; }
if (!defined($arrays_errors[1]))  { $arrays_errors[1]  = "NO"; }
if (!defined($arrays_errors[2]))  { $arrays_errors[3]  = "NO"; }
if (!defined($arrays_errors[3]))  { $arrays_errors[4]  = "NO"; }
if (!defined($arrays_errors[4]))  { $arrays_errors[5]  = "NO"; }
if (!defined($arrays_errors[5]))  { $arrays_errors[6]  = "NO"; }
if (!defined($arrays_errors[6]))  { $arrays_errors[7]  = "NO"; }
if (!defined($arrays_errors[7]))  { $arrays_errors[8]  = "NO"; }
my $bolean = errors_config (\@arrays_errors);
if ( $bolean == 0) { exit; }
# Inputs for Gaussian and Mopac
if (($software=~/gaussian/gi )) {
	print "MESSAGE Choose software Gaussian\n";
} elsif (($software=~/mopac/gi )) {
	print "MESSAGE Choose software Mopac \n";
} elsif (($software=~/adf/gi )) {
	print "MESSAGE Choose software ADF \n";
} else {
	print "ERROR Choose software Gaussian, Mopac or ADF\n";
	exit (1); 
}
#
my @total_atoms = ();
my @new_coord_x = ();
my @new_coord_y = ();
my @new_coord_z = ();
#
my $side = 0;
if ( $Num_of_fragments > 0 ) {
	foreach my $i (@Fragments) {
		(my $word_nospaces = $i) =~ s/\s//g;
		my $filename  = "$word_nospaces.cart";
		my @tmp_array = read_file("$filename");
		foreach my $j (@tmp_array) {
			my @array_tabs  = split (/\s+/,$j);
			my $element = $array_tabs[0];
			push (@new_coord_x,$array_tabs[1]);
			push (@new_coord_y,$array_tabs[2]);
			push (@new_coord_z,$array_tabs[3]);
			#
			if($array_tabs[0] ne "X"){
				push (@total_atoms,$element);
			}
		}
		#dump(@new_coord_x);
		#
		my @array_min = min_vector (\@new_coord_x,\@new_coord_y,\@new_coord_z);
		my @array_max = max_vector (\@new_coord_x,\@new_coord_y,\@new_coord_z);
		my ($xmin,$ymin,$zmin) = @array_min;
		my ($xmax,$ymax,$zmax) = @array_max;
		#
		$side+= Euclidean_distance ($xmin,$ymin,$zmin,$xmax,$ymax,$zmax);		
	}
}
#
if ($Num_of_fragments == 0) {
	print "ERROR please consider atoms and/or fragments";
	exit(1);
} 
# automatic box
my @min_coords = ();
my @max_coords = ();
my ($side_plus_x,$side_plus_y,$side_plus_z);
my $side_box = 0;
if ($option_box == 0) {
	#
	# measure side cube
	#	$side_box   = sprintf '%.3f',($side + 0);	
	#	my $sides = automatic_box_size (\@Atoms);		
	my $sid   = automatic_box_size (\@total_atoms);	
	$side_box = sprintf '%.3f',($side + $sid);
	#
	$side_plus_x  = ($side_box / 2);
	$side_plus_y  = ($side_box / 2);
	$side_plus_z  = ($side_box / 2);		
	#
	my $side_minus = (-1 * $side_plus_x);
	@min_coords    = ($side_minus,$side_minus,$side_minus);
	@max_coords    = ($side_plus_x,$side_plus_y,$side_plus_z);
} else {
	$side_plus_x  = ($Box_x / 2);
	my $side_minus_x = (-1 * $side_plus_x);
	$side_plus_y  = ($Box_y / 2);
	my $side_minus_y = (-1 * $side_plus_y);
	$side_plus_z  = ($Box_z / 2);
	my $side_minus_z = (-1 * $side_plus_z);
	#
	@min_coords    = ($side_minus_x,$side_minus_y,$side_minus_z);
	@max_coords    = ($side_plus_x,$side_plus_y,$side_plus_z);
}
#
my $mi_x = sprintf '%.4f',$min_coords[0];
my $mi_y = sprintf '%.4f',$min_coords[1];
my $mi_z = sprintf '%.4f',$min_coords[2];
my $ma_x = sprintf '%.4f',$max_coords[0];
my $ma_y = sprintf '%.4f',$max_coords[1];
my $ma_z = sprintf '%.4f',$max_coords[2];
#
print "MESSAGE Box size Min = $mi_x $mi_y $mi_z\n"; 
print "MESSAGE Box size Max = $ma_x $ma_y $ma_z\n";
box_molecule (\@min_coords,\@max_coords);
#
foreach my $fil (@Fragments) {
	(my $word_nospaces = $fil) =~ s/\s//g;
	my $filename  = "$word_nospaces.cart";
	my @coordx_At = ();
	my @coordy_At = ();
	my @coordz_At = ();
	my @integral = ();
	my @coordx_Ba = ();
	my @coordy_Ba = ();
	my @coordz_Ba = ();
	my @elements_new = ();
	my @array_file_fix = read_file("$filename");
	foreach my $i (@array_file_fix){
		my @Cartesian = split '\s+', $i;
		#Hasta aca bien Piojo
		#		
		my $element = $Cartesian[0];
		my $radii_val;
		if ($element eq "X"){
			push (@coordx_Ba,$Cartesian[1]);
			push (@coordy_Ba,$Cartesian[2]);
			push (@coordz_Ba,$Cartesian[3]);
			push(@integral, $Cartesian[4]);
		}else{
			push (@coordx_At,$Cartesian[1]);
			push (@coordy_At,$Cartesian[2]);
			push (@coordz_At,$Cartesian[3]);
			if ( exists $Atomic_number{$element} ) {
				# exists
				$radii_val = $Atomic_number{$element};
			} else {
				# not exists
				$radii_val = $element;
			}
			push (@elements_new,$radii_val);
		}
	}
	# call subrutine
	my @array_center_mass = measure_center(\@coordx_At,\@coordy_At,\@coordz_At);
	my @array_vecinvert   = vecinvert(\@array_center_mass);
	# for coords xyz molecules, moveby {x y z} (translate selected atoms)
	my @array_catersian_At   = vecadd (\@coordx_At,\@coordy_At,\@coordz_At,\@array_vecinvert);
	my @array_catersian_Ba   = vecadd (\@coordx_Ba,\@coordy_Ba,\@coordz_Ba,\@array_vecinvert);
	#
	open (my $fh, '>', "$filename") or die "Could not open file '$filename' $!";
	for ( my $i = 0 ; $i < scalar (@coordx_At) ; $i = $i + 1 ){
		print $fh "$elements_new[$i]\t$array_catersian_At[0][$i]\t$array_catersian_At[1][$i]\t$array_catersian_At[2][$i]\n";
	}
	for ( my $i = 0 ; $i < scalar (@integral) ; $i = $i + 1 ){
		print $fh "X\t$array_catersian_Ba[0][$i]\t$array_catersian_Ba[1][$i]\t$array_catersian_Ba[2][$i]\t$integral[$i]\n";
	}
	close $fh;
}
#Hasta aca los cart estan centrados + integral en su basin correcto
#exit(1);




# Fix always first molecule
my $fix_option = 0;
#
if (($fragm_fix=~/YES/gi )) {
	print "MESSAGE Molecule constrain\n";
	$fix_option = 1;
} else { 
	$fix_option = 0;
}
#########
# Iterations
#
my @all_element_coords = (); 
#
my $iteration = 0;
while ( $iteration < $Num_of_geometries_input ) {
	#
	@delete_rot      = ();
	my %hash_tmp     = ();
	my %hash_tmp_basin     = ();
	# filename base
	my $number      = sprintf '%04d', $iteration;
	my $filebase    = "FukMont$number";
	# Obtener el numero total de atomos para un formato XYZ
	my $NumAtoms = scalar (@total_atoms);
	for (my $i = $fix_option; $i < scalar (@Fragments); $i++) {
		(my $frag_id = $Fragments[$i]) =~ s/\s//g;
		open(FRAG,"$frag_id.cart") or die "Unable to open fragment file: $frag_id.cart";
		my @FragLines = <FRAG>;
		close FRAG;
		#
		open(ROT_FRAG,">$frag_id-$i.rot");
		# get a set of angles
		my @base_angles = gen_ptp();
		my $phi         = $base_angles[0];
		my $theta       = $base_angles[1];
		my $psi         = $base_angles[2];
		# do the trig
		my $cos_phi     = sprintf '%.6f', cos($phi);
		my $cos_theta   = sprintf '%.6f', cos($theta);
		my $cos_psi     = sprintf '%.6f', cos($psi);
		my $sin_phi     = sprintf '%.6f', sin($phi);
		my $sin_theta   = sprintf '%.6f', sin($theta);
		my $sin_psi     = sprintf '%.6f', sin($psi);
		# make the rotation matrix
		my $D = new Math::Matrix ([$cos_phi,$sin_phi,0],[-$sin_phi,$cos_phi,0],[0,0,1]);
		my $C = new Math::Matrix ([1,0,0],[0,$cos_theta,$sin_theta],[0,-$sin_theta,$cos_theta]);
		my $B = new Math::Matrix ([$cos_psi,$sin_psi,0],[-$sin_psi,$cos_psi,0],[0,0,1]);
		my $A = $B->multiply($C)->multiply($D);
		#
		while (my $Fline = shift (@FragLines)) {
			my @Cartesians              = split '\s+', $Fline;
			my ($Atom_label, @orig_xyz) = @Cartesians;
			print ROT_FRAG "$Atom_label\t";
			my $matrix_xyz  = new Math::Matrix ([$orig_xyz[0],$orig_xyz[1],$orig_xyz[2]]);
			my $trans_xyz   = ($matrix_xyz->transpose);
			my $rotated_xyz = $A->multiply($trans_xyz);
			my @new_xyz = split '\n+',$rotated_xyz;
			if($Atom_label eq "X"){
				print ROT_FRAG "$new_xyz[0]\t$new_xyz[1]\t$new_xyz[2]\t$orig_xyz[3]\n";
			}else{
				print ROT_FRAG "$new_xyz[0]\t$new_xyz[1]\t$new_xyz[2]\n";
			}
		}
		close ROT_FRAG;		
	}
	#exit(1);
	#
	my $count_h;	
	if ( $fix_option == 1 ) {
		my @read_tmp = read_file ("$Fragments[0].cart");
		$hash_tmp{0} = \@read_tmp;
		$count_h = 1;
	} else { 
		$count_h = 0;
	}
	# Now translate the rotated coords
	for (my $i = $fix_option; $i < scalar (@Fragments); $i++) {
		(my $frag_id = $Fragments[$i]) =~ s/\s//g;
		#
		open(FRAG,"$frag_id-$i.rot") or die "Unable to open fragment file: $frag_id-$i.rot";
		my @FragLines = <FRAG>;
		close (FRAG);
		#
		push (@delete_rot,"$frag_id-$i.rot");
		my @base_xyz  = gen_xyz($side_plus_x,$side_plus_y,$side_plus_z);
		my $base_x    = $base_xyz[0];
		my $base_y    = $base_xyz[1];
		my $base_z    = $base_xyz[2];
		#
		my @all_elem = ();
		my @center_x = (); 
		my @center_y = ();
		my @center_z = ();
		my @Integrals =();	
		#
		my @lots = ();
		my @Basins_Data=();
		#
		while (my $Fline = shift (@FragLines)) {
			my @Cartesians = split '\s+', $Fline;
			my $new_x = $base_x + $Cartesians[1];			
			my $new_y = $base_y + $Cartesians[2];
			my $new_z = $base_z + $Cartesians[3];
			my $integral_saved;

			if($Cartesians[0] eq "X"){	#Basins
				$integral_saved = $Cartesians[4];
				push (@Integrals, $integral_saved);
				push (@Basins_Data,[$new_x,$new_y,$new_z,$integral_saved]);
			}else{	#ATOMS
				$integral_saved = "a";
				push (@all_elem,$Cartesians[0]);
				push (@center_x,$new_x);
				push (@center_y,$new_y);
				push (@center_z,$new_z);
				push (@lots,"$Cartesians[0]\t$new_x\t$new_y\t$new_z");
			}			

		}
		$hash_tmp{$i} = \@lots;
		#$hash_tmp{(scalar @Fragments)+$i}=\@Basins_Data;
		$hash_tmp_basin{$i}=\@Basins_Data;
		$count_h++;
		#print "HERE\n";
		
	}
	
	#
	my $tam_arr_fragments = scalar (@Fragments);
	my $total_mol         = ($tam_arr_fragments);
	#
	my $count_array = 0;
	my %new_hash_1  = ();
	my %new_hash_2  = ();	
	#
	for (my $i = 0; $i < $total_mol; $i++) {
		my @to_coords     = ();
		for (my $j = 0; $j < $total_mol; $j++) {
			if ( $i < $j ){
				$new_hash_1{$count_array} = \@{$hash_tmp{$i}};
				$new_hash_2{$count_array} = \@{$hash_tmp{$j}};
				$count_array++;
			}
		}
		#	
	}
	
	#
	my $add          = 0;
	my $option_check = 0;
#	$option_check = check_inside_box_min_max ( \%hash_tmp,$mi_x,$mi_y,$mi_z);
	$option_check = check_inside_box ( \%hash_tmp,$mi_x,$mi_y,$mi_z);
	$add          = steric_impediment (\%new_hash_1,\%new_hash_2);
#exit(1);
	#
	if ( ( $add == 0 ) && ( $option_check == 0 ) ) {	
		#AQUI A EL CALCULO DE LA INTEGRAL
		my @SumFukui=CIntegralCalculator(\%hash_tmp_basin);
		#@SumFkui tendra el valor de cada Integral por par de fragmentos (1 con el 2, 1 con 3, 2 con 3) y el valor de la suma total como ultimo dato del arreglo $SumaFukui[-1];
		#
		print "\n";
		#exit(1);
		my @new_elem_cart  = ();
		my @new_xyzc_cart  = ();		
		for (my $i = 0; $i < $total_mol; $i++) {
			my @value_array = @{$hash_tmp{$i}};
			foreach my $dn (@value_array) {
				my @info_cart = split '\s+', $dn;
				my $axis_xx  = sprintf '%.6f',$info_cart[1];
				my $axis_yy  = sprintf '%.6f',$info_cart[2];
				my $axis_zz  = sprintf '%.6f',$info_cart[3];			
				push (@new_elem_cart, $info_cart[0]);
				push (@new_xyzc_cart, "$axis_xx  $axis_yy  $axis_zz");
			}	
		}
		
		#
		#
		my $string_element_coords;
		for (my $i = 0; $i < scalar (@new_elem_cart); $i++) {
			$string_element_coords.="$new_elem_cart[$i]  $new_xyzc_cart[$i]\n";
		}
		$num_atoms_xyz = $NumAtoms;
		push (@all_element_coords,$string_element_coords);
		push (@arrayOutputs,$filebase);
		$iteration++;
	}
	
}

# En esta parte va lo de lo de duplicados
print "MESSAGE Find Structures Similar\n";
#exit(1);
my %structure_info = %{info_duplicate_structures ($Num_of_geometries_input,$num_atoms_xyz,\@all_element_coords,$nprocess)};
my @keys_array     = keys %structure_info;
#
my @final_coords   = (); 
#
foreach my $my_key (sort(keys %structure_info)) {
	my @array_coords_hash = @{$structure_info{$my_key}};
	my $str = join ("\n", @array_coords_hash);
	push (@final_coords,$str);
}
#
my $option_soft = 0;
for ( my $i=0; $i < scalar (@final_coords); $i++) {
	chomp ($final_coords[$i]);
	if (($software=~/gaussian/gi )) {
		G03Input ($arrayOutputs[$i],$header,$ncpus,$mem,$Charge,$Multiplicity,$final_coords[$i],$i);		
	}
	if (($software=~/mopac/gi )) {
		$option_soft = 3;
		my $MopacInput = MopacInput ($arrayOutputs[$i],$final_coords[$i],$i,$header,$Charge,$Multiplicity,$ncpus,$mem);
		system ("$path_bin_mopac $MopacInput >tmp_mopac_1.txt 2>tmp_mopac_2.txt");
	}
}
#
my $tiempo_final = new Benchmark;
my $tiempo_total = timediff($tiempo_final, $tiempo_inicial);
print "MESSAGE Execution time: ",timestr($tiempo_total),"\n";

