#!/bin/bash/

echo "Super Script is Running....\n"

echo "Names of the files (pre numbers):"

read name

echo "Special separation char (_ , ; . -):";

read sep

echo "Files (sorted) are:";
# ls the files, sort them. -n is numeric sort, -t is a special char (divide the filename in 2 "names") and -k choose the "name" to sort.
ls "$name"*.out | sort -n -t "$sep" -k 2
 
echo "\n"
echo "File 			Y-component" > "$name"_resume.txt
#rm "$name"_resume.txt
 for result in $(ls "$name"*.out | sort -n -t "$sep" -k 2);
 do
 	grep -A 2 'scalar              x-component         y-component         z-component' "$result" | tail -1 > tmp.tmp
 	# the number $3 in the awk is the column with data to get
 	value=$(awk '{print $3}' tmp.tmp )
 	number=$(echo "$result" | awk  -F '[_"'"$sep"'"]' '{print $2}')

 	#Write file -data
 	#echo "$number"
 	#CAMBIAR ACA LO Q SE MUESTRA EN EL TXT
 	echo "$number 			$value" >> "$name"_resume.txt

done
rm tmp.tmp