#!/usr/bin/perl

use strict;
use warnings; no warnings 'uninitialized';
#use bignum;
use Data::Dump qw(dump ddx);



my $totalNumberVectors=0;
#my $sumVectors=0;
my $sumPos=0;
my $sumNeg=0;
my $factor =28.17909;
###################################
#

############################+
my $vectorFile= $ARGV[0];
my $Line_Vector= $ARGV[1];
my $direction = $ARGV[2];

if($#ARGV != 2){
	print "Numero incorrecto de argumentos, usar:\n $0 [FILE] [LINEVECTOR] [DIRECTION]\n\nDIRECTION 1 X; 2 Y; 3 Z\n";
	exit(1);
}
sub CrossProductVector{
	my ($x1, $y1, $z1, $x2, $y2, $z2)=@_;
	dump @_;
	my $d1= ($y1*$z2)-($z1*$y2);
	my $d2= (($x1*$z2)-($z1*$x2)) * (-1);
	my $d3= ($x1*$y2)-($y1*$x2);
	return ($d1, $d2, $d3);

}
sub DotProductVector{
	my ($x1, $y1, $z1, $x2, $y2, $z2)=@_;
	my $d1= $x1*$x2;
	my $d2= $y1*$y2;
	my $d3= $z1*$z2;
	my $suma = $d1+$d2+ $d3;
	return $suma;
}
sub VeritfyCoplanar{
	my ($vector, $totalVectors)=@_;
	my @allvector=@{$vector};
	print "works with $allvector[0]";
	#primero haemos el producto cruz entre 1er vector y 2do vector
	my @first= split( " ", $allvector[0]);
	my @second= split( " ", $allvector[1]);
	my @normal = CrossProductVector($first[0], $first[1], $first[2],
									$second[0], $second[1], $second[2]);
	dump @normal;
	foreach my $i (2..$totalVectors){
		my @tmp = split(" ", $allvector[$i]);
		#dump @tmp;
		my $dot = abs($normal[0]*$tmp[0]) + ($normal[1]*$tmp[1]) + ($normal[2]*$tmp[2]);
		printf "%.3f\n",$dot;
		#exit(1);
	}

}

open(DATA,"$vectorFile\n");
my @data= <DATA>;
close(DATA);
#dump @data;
my $totalVectors = ($data[$Line_Vector])*1;
print "numero es $totalVectors\n";
my @new= splice @data, $Line_Vector+1, $totalVectors;
#dump @new;
print "perimero $new[0]\tUltimo: $new[-1]";
#VeritfyCoplanar(\@new, $totalVectors);
for (my $j = 0; $j < $totalVectors; $j++) {
	my @tmp1 = split(" ", $new[$j]);
	for (my $h = $j; $h < $totalVectors; $h++) {
		my @tmp2 = split(" ", $new[$h]);
		my $sum = DotProductVector($tmp1[0], $tmp1[1], $tmp1[2], 
									$tmp2[0], $tmp2[1], $tmp2[2]);
		print "ideal es 0? = $sum\n";
	}
}

 #   return \@parts;
#



#####OLD CODE WORKS SOMEHOW
# open(DATA,"$vectorFile\n");
# my $aux=1;
# foreach my $line (<DATA>){
# 	if ($aux == $Line_Vector -1){
# 		$totalNumberVectors= ($line)*1;
# 		print "total es $totalNumberVectors\n";
# 	}	
# 	if ($aux >= $Line_Vector+$totalNumberVectors){
# 		print "End of line.\nLast: $line\n";
# 		last;
# 	}elsif ($aux >= $Line_Vector){
# 		#print "$totalNumberVectors\n $line\n";
# 		#print "Working on Vector $aux\n";
# 		my @tmp=split(" ",$line);
# 		my $moduleVector= sqrt($tmp[0]**2 + $tmp[1]**2 + $tmp[2]**2);
# 		if( $tmp[$direction-1] >0 ){	#positivos
# 			#$sumPos= $sumPos+ abs($tmp[$direction-1]);
# 			$sumPos= $sumPos+ $moduleVector;
# 		}elsif( $tmp[$direction-1] <0 ){							#negativos
# 			#$sumNeg= $sumNeg+ abs($tmp[$direction-1]);
# 			$sumNeg= $sumNeg+ $moduleVector;
# 		}

# 		#print "Module: $moduleVector\n";
# 		#$sumVectors+=$moduleVector;
# 		#exit;
# 	}
# 	$aux++;
# }
# close(DATA);
# my $resta= $sumPos- $sumNeg;
# printf "Suma Direcion Positiva: %.8f\n", ($sumPos * $factor);
# printf "Suma Direcion Negativa: %.8f\n", ($sumNeg * $factor);
# printf "Resta: %.8f\n", ($resta*$factor);