#!/usr/bin/env perl
#!/usr/bin/perl -s

use strict;
use Data::Dump qw(dump ddx);

my %atom_counting = (default=>0);

sub ReadXYZFile{
	my ($file, $tmpOption)=@_;
	my @at = ();
	my @cx = ();
	my @cy = ();
	my @cz = ();

	open(FILE, "$file");
	my @lines=<FILE>;
	close(FILE);
	shift(@lines); #primera linea tiene el numero de atomos.
	shift(@lines); #segunda linea tiene el titulo
	#demas lineas tienen las coordedanas.
	foreach my $line(@lines){
		my ($a, $x, $y, $z)=split(" ", $line);
		#This option works only for 1 recombination, more than 1 will duplicate the amount of maximmum atoms in memory
		if ($tmpOption==1) {$atom_counting{$a}++;$atom_counting{"all"}++;}
		push @at, $a;
		push @cx, $x;
		push @cy, $y;
		push @cz, $z;
	}
	#dump(%atom_counting);
	my @coords=([@at],
				[@cx],
				[@cy],
				[@cz]);
	return @coords;

}
sub ReadFreqTer{
	my($file)=@_;
	open(my $FILE,"$file");
	#my @lines=<FILE>;
	my $flagStart=0;
	my $flagY=0;
	my $aux=1;
	#
	my @fx=();
	my @fy=();
	my @fz=();
	while (my $line=<$FILE>) {
		#if($key=~/fragment1/i){
		if($line=~/----/){
			$flagStart=1;
			print "\n";
		}elsif($flagStart==1){
			my @tmpline=split(" ",$line);
			if(scalar(@tmpline)==5){ # this means new atom and X coordinate
				#print "$aux\tX es $tmpline[1]\t";
				$flagY=1;
				push @fx, $tmpline[1];
			}elsif($flagY==1 && scalar(@tmpline)==4){
				# Y coordinate
				#print "Y es $tmpline[0]\t";
				$flagY=0;
				push @fy, $tmpline[0];
			}elsif(scalar(@tmpline)==4){
				# Z coordinate
				#print "Z es $tmpline[0]\n";
				push @fz, $tmpline[0];
				$aux++;
			}else{
				last;
			}
		}
	}
	close($FILE);
	my @sumCoords=([@fx],
					[@fy],
					[@fz]);
	return @sumCoords;
}


if($#ARGV<2){
	print "Use\n";
	print "$0 [Original XYZ file] [Terachem Freq File] [Output name]\n";
	exit(1);
}


my $file = $ARGV[0];
my @structure = ReadXYZFile($file,1);
#dump(%atom_counting);
my @frecoords=ReadFreqTer($ARGV[1]);

open(NEW, ">$ARGV[2]");
print NEW "$atom_counting{all}\n$file + frecuencias Sumadas\n";
for (my $i = 0; $i < $atom_counting{all}; $i++) {
	my $new_x=$structure[1][$i]+$frecoords[0][$i];
	my $new_y=$structure[2][$i]+$frecoords[1][$i];
	my $new_z=$structure[3][$i]+$frecoords[2][$i];
	#print "combinacoin: $structure[1][$i]+$frecoords[0][$i] = $new_x\n";
	#last;
	print NEW "$structure[0][$i]\t$new_x\t$new_y\t$new_z\n";
}
print "DONE\n";