#!/usr/bin/perl

use strict;
use warnings; no warnings 'uninitialized';
use Data::Dump qw(dump ddx);

my $datFile;
############
my $atomTotal = 0;
my $Xl        =-1;
my $Yl        =-1;
my $Zl        =-1;
my $delta;
my $del_x;
my $del_y;
my $del_z;
my $NumPts;
my $NX;
my $NY;
my $NZ;
my $origen_x;
my $origen_y;
my $origen_z;
my $out;
my ($type, $option);

###################################
# Delete whitespaces from Strings
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };

sub convert_armstrong_to_bohr{ 
	my $s     = shift; 
	my $multi = $s/0.529177249; 
	return $multi;
}
###################################
#
sub ReadDatGetInfo{
	my($same1, $same2, $ori1, $ori2, $datFile)=@_;
	#tie my @meshdata, 'Tie::File', $datFile;
	#
	open(VALUES, "$datFile");
	my @meshdata = <VALUES>;
	close(VALUES);
	#
	my %magicalLines;
	my $aux      = 0;
	foreach my $data (@meshdata){
		my @information=split(" ",$data);
		# print "$information[$same1]\n";
		if($information[$same1]==$ori1 && $information[$same2]==$ori2 && $information[0] eq "Bq"){
			# print "$information[0]\t$information[1]\t$information[2]\t$information[3]\n";
			$magicalLines{$aux}=[@information];
			$aux++;
		}elsif($information[0] ne "Bq"){
			$magicalLines{$aux}=[@information];
			$aux++;
		}
	}
	undef @meshdata;
	return %magicalLines;
}
###################################
#
sub FIPC{
	#
	# Simulando que SIEMPRE es eje Z en 0 Angstrom
	my($type, $outputName, $datFile)=@_;
	my %magicalLines;
	my $outputShow;
	my $PrintC;
	my $PrintCC;
	#
	%magicalLines = ReadDatGetInfo(1,2,0, 0,$datFile);
	$outputShow   = 3;
	$PrintC       = "Z";
	$PrintCC      = "ZZ";
	#
	my $total     = keys %magicalLines;
	#
	if($total!=0){
		# Wirte file 
		open(FIPC, ">$outputName.txt");
		if( $type == 1 ){
			print FIPC "Type\t$PrintC\tXX\tYY\tZZ\tIn-Plane\tOut-Plane\n\n";
		}elsif( $type == 2 ){
			print FIPC "Type\t$PrintC\t$PrintCC\tIso\tAni\n\n";
		}else{
			print FIPC "Type\t$PrintC\tXX\tYY\tZZ\tIso\tIn-Plane\tOut-Plane\tAni\n\n";
		}
		#
		foreach my $id (0..$total-1){
			my $newXX = ${$magicalLines{$id}}[6]*(-1);
			my $newYY = ${$magicalLines{$id}}[7]*(-1);
			my $newZZ = ${$magicalLines{$id}}[8]*(-1);
			my $inplane;
			my $outplane;
			$inplane  = ($newZZ+$newYY)/3;
			$outplane = $newXX/3;
			print  FIPC "${$magicalLines{$id}}[0]\t";
			printf FIPC ("%.3f\t",${$magicalLines{$id}}[$outputShow]);
			if( $type == 1 ){
				printf FIPC ("%.3f\t",$newXX);
				printf FIPC ("%.3f\t",$newYY);
				printf FIPC ("%.3f\t",$newZZ);
				printf FIPC ("%.3f\t",$inplane);
				printf FIPC ("%.3f\n",$outplane);
			}elsif( $type == 2 ){
				printf FIPC ("%.3f\t",${$magicalLines{$id}}[$outputShow+5]*(-1));
				printf FIPC ("%.3f\t",${$magicalLines{$id}}[4]);
				printf FIPC ("%.3f\n",${$magicalLines{$id}}[5]);
			}else{
				printf FIPC ("%.3f\t",$newXX);
				printf FIPC ("%.3f\t",$newYY);
				printf FIPC ("%.3f\t",$newZZ);
				printf FIPC ("%.3f\t",${$magicalLines{$id}}[4]);
				printf FIPC ("%.3f\t",$inplane);
				printf FIPC ("%.3f\t",$outplane);
				printf FIPC ("%.3f\n",${$magicalLines{$id}}[5]);
			}
		}
		close(FIPC);
	}
}
###################################
#
sub GetParamOFDat{
	#tie my @meshdata, 'Tie::File', $datFile;
	my($datFile)=@_;
	open(OUT, "$datFile");
	my @meshdata=<OUT>;
	close(OUT);
	#
	foreach my $x (@meshdata) {
		my @tmp = split(" ",$x);
		# atoms are over and now Bq data starts
		if($tmp[0] eq "Bq"){	
			last;
		}
		$atomTotal++;
	}
	# Last line of .backup file has: "X Y Z delta NumX NumY NumZ". 
	# Meaning x,y,z coordinates of origin (corner); separation between points Angstron; 
	# and number of points in each axis.
	my @meshSizeData  = split(" ",$meshdata[-1]);
	#
	$origen_x = $meshSizeData[0];
	$origen_y = $meshSizeData[1];
	$origen_z = $meshSizeData[2];
	$del_x = $del_y = $del_z = $delta  = $meshSizeData[3];
	$Xl = $meshSizeData[4];
	$Yl = $meshSizeData[5];
	$Zl = $meshSizeData[6];
	$NumPts = $Xl * $Yl * $Zl;

	if($Xl==1){
		$del_x = 0;
	}
	if($Yl==1){
		$del_y = 0;
	}
	if($Zl==1){
		$del_z = 0;
	}
	
}
###################################

sub CreateVTIFileHeader{
	my($outputName, $id) = @_;
	open (VTI,">$outputName\_$id.vti");
	print VTI "<?xml version=\"1.0\"?>\n";
	print VTI "<VTKFile type=\"ImageData\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
	print VTI "\t<ImageData WholeExtent=\"";
	print VTI " 0 ".($Xl-1);
	print VTI " 0 ".($Yl-1);
	print VTI " 0 ".($Zl-1)."\"";
	print VTI " Origin=\"";
	print VTI "$origen_x $origen_y $origen_z\"";
	print VTI " Spacing=\"";
	print VTI "$del_x $del_y $del_z\">\n";	
	print VTI "\t<Piece Extent=\"";
	print VTI " 0 ".($Xl-1);
	print VTI " 0 ".($Yl-1);
	print VTI " 0 ".($Zl-1)."\">\n";
	print VTI "\t<PointData Scalars=\"scalars\">\n";
	print VTI "\t<DataArray Name=\"vectors\" type=\"Float64\" NumberOfComponents=\"3\" Format=\"ascii\">\n";
	close(VTI);
}
# Build file in .cube file for the ZZ value.
sub CreateCubeFile{
	my($valueToWrite, $outputName, $datFile) = @_;
	#
	open(DATA,"$datFile");
	my @meshdata = <DATA>;
	close(DATA);

	CreateVTIFileHeader($outputName,"Pos1");
	CreateVTIFileHeader($outputName,"Neg1");

	# CUBE header information here
	open(CUBE, ">$outputName.cube");
	print CUBE "Cube File generated by Magnetum\n";
	print CUBE "Totally\t$NumPts grid points\n";
	print CUBE "$atomTotal\t",	convert_armstrong_to_bohr($origen_x),"\t",
								convert_armstrong_to_bohr($origen_y),"\t",
								convert_armstrong_to_bohr($origen_z),"\n";
	printf CUBE ("$Xl\t%.6f\t0.000000\t0.000000\n",convert_armstrong_to_bohr($delta));
	printf CUBE ("$Yl\t0.000000\t%.6f\t0.000000\n",convert_armstrong_to_bohr($delta));
	printf CUBE ("$Zl\t0.000000\t0.000000\t%.6f\n",convert_armstrong_to_bohr($delta));
	# atom data in Bhor here
	for (my $i = 0 ; $i < $atomTotal ; $i++) {
		my @tmp = split(" ",$meshdata[$i]);
		printf CUBE ("$tmp[0]\t0.0\t%.6f\t%.6f\t%.6f\n",convert_armstrong_to_bohr($tmp[1]),
												convert_armstrong_to_bohr($tmp[2]),
												convert_armstrong_to_bohr($tmp[3]))
	}

	open(VTIPOS,">>$outputName\_Pos1.vti");
	open(VTINEG,">>$outputName\_Neg1.vti");

	my $aux = $atomTotal;
	# CUBE Information here
	for (my $i = 0; $i < $Xl; $i++) {
		for (my $j = 0; $j < $Yl; $j++) {
			my $flag=1;
			my $written = 0;
			for (my $k = 0; $k < $Zl; $k++) {
				my @tmp = split(" ",$meshdata[$aux]);
				#use bignum;
				my $legalX= sprintf "%.6f",($i*$delta)+$origen_x;
				my $legalY= sprintf "%.6f",($j*$delta)+$origen_y;
				my $legalZ= sprintf "%.6f",($k*$delta)+$origen_z;

				$flag=1;
				my $vx=0;
	    		my $vy=0;
	    		my $vz=0;
				if( $legalX == $tmp[1] && $legalY == $tmp[2] && $legalZ == $tmp[3]){
					printf CUBE ("%.6f\t",($tmp[$valueToWrite])*(-1));	
					$written++;
					$aux++;
					#Control VTI
					$vx = $tmp[-2]*(-1);
					$vy = $tmp[-1]*(-1);
					$vz = $tmp[-3]*(-1);
					# En caso los vectores se vean muy peqeuños jugar con esta variable
					my $module= sqrt(($vx**2) + ($vy**2) + ($vz**2));
					if($vz > 0){ 					#Pos values here
						print VTIPOS "\t\t$vx\t$vy\t$vz\n";
						print VTINEG "\t\t0.0000\t0.0000\t0.0000\n";
					}else{							#Neg values here (and 0)
						print VTIPOS "\t\t0.0000\t0.0000\t0.0000\n";
						print VTINEG "\t\t$vx\t$vy\t$vz\n";
					}
				}else{
					print CUBE "0.000000\t";
				}
				if($written%6 == 0){
					print CUBE "\n";
					$flag=0;
					$written=0;
				}
			}
			if($flag!=0){
				print CUBE "\n";
			}
		}
		my $pcent = (($i+1)*100/$Xl);
		printf "Writting %.2f%s of cube\n",$pcent, '%';
	}
	close(VTIPOS);
	close(VTINEG);
	RectifyVTI("$outputName\_Pos1.vti","$outputName\_Pos.vti");
	RectifyVTI("$outputName\_Neg1.vti","$outputName\_Neg.vti");
	close(CUBE);
}
sub RectifyVTI{
	my ($vtiFile, $newName)=@_;

	my $posX = $Yl * $Zl;
    my $posY = $Zl;
    my $header = 6; 
	open(VTI, "$vtiFile");
	my @line = <VTI>;
	close(VTI);
	open(LEGAL, ">$newName");
	foreach my $i (1..$header){
		print LEGAL "$line[$i]";
	}
	for (my $z = 0; $z < $Zl; $z++) {
		for (my $y = 0; $y < $Yl; $y++) {
			for (my $x = 0; $x < $Xl; $x++) {
				my $key = ($posX*$x) + ($posY*$y) + $z + $header;		#Ubica y traslada informacion
				print LEGAL "$line[$key]";
			}
		}
	}
	print LEGAL "\n\t</DataArray>\n\t</PointData>\n\t\t</Piece>\n\t</ImageData>\n</VTKFile>\n";
	close(LEGAL);
	unlink $vtiFile;
	
}
###################################
#
sub SetVariables{
	my($configFile)=@_;
	open(CONFIG, "$configFile");
	my ($outputName,$type,$option);
	foreach my $line (<CONFIG>){
		chomp($line);
		if( $line=~/coords/i    ) {
			$outputName=(split("=",$line))[-1];
		}
		if( $line=~/type_graph/i) {
			$type=(split("=",$line))[-1];
		}
		if ( $line=~/option/i   ) {
			$option=(split("=",$line))[-1];
		}
	}
	return ($outputName,$type,$option);
}
###################################
# This function sets the variables to print in the .cube and .vtk files
sub Starts{
	my($datFile, $surname, $outputName)=@_;
	GetParamOFDat($datFile);
	if($type==1){
		$outputName=$outputName.$surname."_ISO";
	}elsif($type==2){
		$outputName=$outputName.$surname."_ANI";
	}elsif($type==3){
		$outputName=$outputName.$surname."_XX";
	}elsif($type==4){
		$outputName=$outputName.$surname."_YY";
	}elsif($type==5){
		$outputName=$outputName.$surname."_ZZ";
	}elsif($type==6){
		$outputName=$outputName.$surname."_FIPC";
	}elsif($type==7){
		$outputName=$outputName.$surname."_SCANS";
	}elsif($type==8){
		$outputName=$outputName.$surname."_FS";
	}
	# 
	if($type==6){
		print "MESSAGE Creating FIPC file\n";
		FIPC(1,$outputName,$datFile);
	}elsif($type==7){
		#SCAN
		print "MESSAGE Creating SCANS\n";
		FIPC(2,$outputName,$datFile);
	}elsif($type==8){
		print "MESSAGE Creating FIPC and SCANS File\n";
		FIPC(3,$outputName,$datFile);
	}else{
		CreateCubeFile(($type+3), $outputName,$datFile);
	}
}

my $configFile= $ARGV[0];
(my $basename, $type, $option)=SetVariables($configFile);
(my $outputName = $basename) =~ s/\.[^.]+$//;
$outputName=trim($outputName);


if($option==0){
	$datFile="ValuesICSS.backup";
	Starts($datFile,"",$outputName);
}else{
	$datFile="ValuesICSS_SIGMA.backup";
	Starts($datFile,"SIG",$outputName);
	#Resets the global variables, necesary for SigmaPi calculation
	$atomTotal=0;
	$Xl=-1;
	$Yl=-1;
	$Zl=-1;
	$datFile="ValuesICSS_PI.backup";
	Starts($datFile,"PI",$outputName);
}
