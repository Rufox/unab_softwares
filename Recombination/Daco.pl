#!/usr/bin/env perl

use strict;
use warnings;
use Parallel::ForkManager;
use Benchmark; #entrega cuando demora el proceso, cuanto de CPU utilizó, etc
# Global variable
my $numb_atoms;
# Global array
my @tmp_new_neighbors_atoms = ();
#
my $other_element = 0.8;
my %Atomic_radii = ( 'H'  => '0.4', 'He' => '0.3', 'Li' => '1.3', 'Be' => '0.9', 
                     'B'  => '0.8', 'C'  => '0.8', 'N'  => '0.8', 'O'  => '0.7', 
					 'F'  => '0.7', 'Ne' => '0.7', 'Na' => '1.5', 'Mg' => '1.3', 
					 'Al' => '1.2', 'Si' => '1.1', 'P'  => '1.1', 'S'  => '1.0', 
					 'Cl' => '1.0', 'Ar' => '1.0', 'K'  => '2.0', 'Ca' => '1.7', 
					 'Sc' => '1.4', 'Ti' => '1.4', 'V'  => '1.3', 'Cr' => '1.3', 
					 'Mn' => '1.4', 'Fe' => '1.3', 'Co' => '1.3', 'Ni' => '1.2', 
					 'Cu' => '1.4', 'Zn' => '1.3', 'Ga' => '1.3', 'Ge' => '1.2', 
					 'As' => '1.2', 'Se' => '1.2', 'Br' => '1.1', 'Kr' => '1.1', 
					 'Rb' => '2.1', 'Sr' => '1.9', 'Y'  => '1.6', 'Zr' => '1.5', 
					 'Nb' => '1.4', 'Au' => '1.4' );

my %Atomic_number = ( '89'  => 'Ac', '13'  => 'Al', '95'  => 'Am', '51'  => 'Sb',	
	                  '18'  => 'Ar', '33'  => 'As', '85'  => 'At', '16'  => 'S',  
					  '56'  => 'Ba', '4'   => 'Be', '97'  => 'Bk', '83'  => 'Bi',	
                      '107' => 'Bh', '5'   => 'B', 	'35'  => 'Br', '48'  => 'Cd',	
	                  '20'  => 'Ca', '98'  => 'Cf',	'6'   => 'C',  '58'  => 'Ce',	
	                  '55'  => 'Cs', '17'  => 'Cl',	'27'  => 'Co', '29'  => 'Cu',	
	                  '24'  => 'Cr', '96'  => 'Cm', '110' => 'Ds', '66'  => 'Dy',
	                  '105' => 'Db', '99'  => 'Es', '68'  => 'Er', '21'  => 'Sc',	
	                  '50'  => 'Sn', '38'  => 'Sr', '63'  => 'Eu', '100' => 'Fm',	
	                  '9'   => 'F',  '15'  => 'P',  '87'  => 'Fr', '64'  => 'Gd',	
	                  '31'  => 'Ga', '32'  => 'Ge', '72'  => 'Hf', '108' => 'Hs',	
                      '2'   => 'He', '1'   => 'H',  '26'  => 'Fe', '67'  => 'Ho',	
					  '49'  => 'In', '53'  => 'I',  '77'  => 'Ir', '70'  => 'Yb',
					  '39'  => 'Y',  '36'  => 'Kr', '57'  => 'La', '103' => 'Lr',	
					  '3'   => 'Li', '71'  => 'Lu', '12'  => 'Mg', '25'  => 'Mn',	
                      '109' => 'Mt', '101' => 'Md', '80'  => 'Hg', '42'  => 'Mo',	
					  '60'  => 'Nd', '10'  => 'Ne', '93'  => 'Np', '41'  => 'Nb',	
					  '28'  => 'Ni', '7'   => 'N',  '102' => 'No', '79'  => 'Au',	
					  '76'  => 'Os', '8'   => 'O', 	'46'  => 'Pd', '47'  => 'Ag',	
					  '78'  => 'Pt', '82'  => 'Pb',	'94'  => 'Pu', '84'  => 'Po',	
					  '19'  => 'K',  '59'  => 'Pr', '61'  => 'Pm', '91'  => 'Pa',	
					  '88'  => 'Ra', '86'  => 'Rn', '75'  => 'Re', '45'  => 'Rh',	
					  '37'  => 'Rb', '44'  => 'Ru', '104' => 'Rf', '62'  => 'Sm',
					  '106' => 'Sg', '34'  => 'Se', '14'  => 'Si', '11'  => 'Na',
					  '81'  => 'Tl', '73'  => 'Ta', '43'  => 'Tc', '52'  => 'Te',	
					  '65'  => 'Tb', '22'  => 'Ti', '90'  => 'Th', '69'  => 'Tm',	
					  '112' => 'Uub','116' => 'Uuh','111' => 'Uuu','118' => 'Uuo',	
					  '115' => 'Uup','114' => 'Uuq','117' => 'Uus','113' => 'Uut',
					  '92'  => 'U',  '23'  => 'V',  '74'  => 'W',  '54'  => 'Xe',
                      '30'  => 'Zn', '40'  => 'Zr' );


					  
###################################
# Verification
sub verification{
	my ($a1, $a2, $dist)=@_;
	# hash values	
	my $v1  = $Atomic_radii{$a1} || $other_element; 
	my $v2  = $Atomic_radii{$a2} || $other_element;
	my $sum = $v1 + $v2;  
	my $resultado;
	# steric effects if radio1+radio2 < distance
	if($dist <= $sum){
		# Steric problem	
		$resultado = 1; 
	}else{
		$resultado = 0;
	}
	return $resultado;
}
###################################
# Steric Impediment for atoms
sub steric_impediment_atoms{
	# array are send by reference
	my ($hash_tmp) = @_;
	# get size
	my $final_trial = 0;
	my $resultado   = 0;
	my @total       = @{$hash_tmp};
	#
	for (my $i=0; $i < scalar(@total);$i++){
		for (my $j=0; $j < scalar(@total); $j++){
			if ( $i < $j ){
				my ($atom_1,$axis_x_1,$axis_y_1,$axis_z_1) = split '\s+', $total[$i];
				my ($atom_2,$axis_x_2,$axis_y_2,$axis_z_2) = split '\s+', $total[$j];					
				my $distance    = Euclidean_distance($axis_x_1,$axis_y_1,$axis_z_1,$axis_x_2,$axis_y_2,$axis_z_2);					
				$final_trial = $final_trial + verification($atom_1,$atom_2,$distance);
				if( $final_trial ==	 1 ) {
					$resultado = 1;
					last;
				}
			}
		}
	}
	# verify for steric impediment, 1 yes, 0 no;
	return $resultado;		
}

###################################
# compute the center of mass
sub measure_center {
	my ($coord_x,$coord_y,$coord_z) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array  = ();
	my $weight = 1;
	# variable sum
	my $sum_weight = 0;
	my $sum_x = 0;
	my $sum_y = 0;
	my $sum_z = 0;
	for ( my $j = 0 ; $j < $num_data ; $j = $j + 1 ){
		$sum_weight+= $weight;
		$sum_x+= $weight * @$coord_x[$j];
		$sum_y+= $weight * @$coord_y[$j];
		$sum_z+= $weight * @$coord_z[$j];		
	}
	my $com_x = $sum_x / $sum_weight;
	my $com_y = $sum_y / $sum_weight;
	my $com_z = $sum_z / $sum_weight;
	# array
	@array = ($com_x,$com_y,$com_z);
	# return array	
	return @array;
}
###################################
# Returns the additive inverse of v(-v)
sub vecinvert {
	my ($center_mass) = @_;
	my @array         = ();
	foreach my $i (@$center_mass) {
		my $invert        = $i * -1;
		$array[++$#array] = $invert; 
	}	
	# return array	
	return @array;
}
###################################
# Returns the vector sum of all the terms.
sub vecadd {
	my ($coord_x,$coord_y,$coord_z,$vecinvert_cm ) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array   = ();
	my $sum_coord_x;
	my $sum_coord_y;
	my $sum_coord_z;
	# array 
	my @array_x = ();
	my @array_y = ();
	my @array_z = ();
	for ( my $i = 0 ; $i < $num_data ; $i = $i + 1 ){	
		$sum_coord_x = @$coord_x[$i]+@$vecinvert_cm[0] ; 
		$sum_coord_y = @$coord_y[$i]+@$vecinvert_cm[1] ;
		$sum_coord_z = @$coord_z[$i]+@$vecinvert_cm[2] ;
		# save array
		$array_x[++$#array_x] = $sum_coord_x;
		$array_y[++$#array_y] = $sum_coord_y;
		$array_z[++$#array_z] = $sum_coord_z;
	}
	@array = ( [@array_x], 
              [@array_y], 
              [@array_z] ); 
	# return array	
	return @array;
}



###################################
# value covalent radii
sub value_covalent_radii {
	my ($element) = @_;
	my $radii_val = 0;
	if ( exists $Atomic_radii{$element} ) {
		$radii_val = $Atomic_radii{$element};
	} else {
		$radii_val = $other_element ;
	}	
	return $radii_val
}
###################################
# Automatic box length
sub automatic_box_length {
	my ($input_array) = @_;
	my $sum = 0;
	#
	foreach my $i (@{$input_array}) {
		my $radii_val;
		if ( exists $Atomic_radii{$i} ) {
			# exists
			$radii_val = $Atomic_radii{$i};
		} else {
			# not exists
			$radii_val = $other_element ;
		}
		$sum+=$radii_val;
	}
	return $sum;
}
###################################
# Euclidean distance between points
sub Euclidean_distance {
	# array coords basin 1 and basin 2
	my ($p1,$p2,$p3, $axis_x, $axis_y, $axis_z) = @_;
	# variables
	my $x1 = $axis_x;
	my $y1 = $axis_y;
	my $z1 = $axis_z;
	# measure distance between two point
	my $dist = sqrt(
					($x1-$p1)**2 +
					($y1-$p2)**2 +
					($z1-$p3)**2
					); 
	return $dist;
}
###################################
# function to determine if a string is numeric
sub looks_like_number {
	my ($array_input) = @_;	
	#
	my $number;
	my $element;
	my @array_elements = ();
	for ( my $i=2 ; $i < scalar (@{$array_input}); $i++) {
		$number  = 0;
		if ( @$array_input[$i] =~ /^[0-9,.E]+$/ ) {
			$number = @$array_input[$i];
		} else {
			$element = @$array_input[$i];
		}
		for ( my $j = 0 ; $j < $number ; $j++) {
			push (@array_elements,$element);
		}
	}
	return @array_elements;
}
###################################
# fisher yates shuffle
sub fisher_yates_shuffle {
	my $array = shift;
	my $i = @$array;
	while ( --$i ) {
		my $j = int rand( $i+1 );
		@$array[$i,$j] = @$array[$j,$i];
	}
	return @$array; 
}
###################################
# Construct Discrete Search Space Cube
sub Construct_Discrete_Search_Space_Cube {
	my ($length_side_box,$cell_size_w) = @_;
	#
	my $max_coord            = $length_side_box;
	my $cell_center          = ( $cell_size_w / 2 ); 
	my $total_number_of_cell = ( $max_coord / $cell_size_w );
	my @discretized_search_space = ();
	#
	my $num_tot = 0;
	for (my $x=0; $x < $total_number_of_cell ; $x++) {
		for (my $y=0; $y < $total_number_of_cell ; $y++) { 
			for (my $z=0; $z < $total_number_of_cell ; $z++) { 
				my $coord_x = ( ( $x * $cell_size_w ) + $cell_center );
				my $coord_y = ( ( $y * $cell_size_w ) + $cell_center );
				my $coord_z = ( ( $z * $cell_size_w ) + $cell_center );
				$discretized_search_space[++$#discretized_search_space] = "$coord_x\t$coord_y\t$coord_z";
				$num_tot++;
			}
		}
	}
	#
	my $file_tmp = "space.xyz";
	open (FILE, ">$file_tmp") or die "Unable to open XYZ file: $file_tmp";	
	print FILE "$num_tot\n\n";
	my @cube_space = Center_All_Cell (\@discretized_search_space,0);
	foreach (@cube_space) {
		print FILE "X\t$_\n";
	}
	close (FILE);
	#
	return @discretized_search_space;
}
###################################
# Construct Solutions
sub Construct_Solutions {
	my ($number_atoms,$Atoms_set,$discretized_search_space) = @_;
	#
	my @array_all_atoms      = @{$Atoms_set};
	# Delete construct solutions for atoms
	@tmp_new_neighbors_atoms = ();
	#
	for (my $i=0; $i < $number_atoms ; $i++) {
		my $element_neighbors = $i;
		# Este >= y <= va si o si
		if ( $element_neighbors >= 1) {
			my @array_neighbors = ();
			for (my $j=0; $j <= $element_neighbors ; $j++) {
				push (@array_neighbors,$array_all_atoms[$j]);
			}
			Rules_Cell_Automata (\@array_neighbors,\@{$discretized_search_space});
			#
		}
	}
	# return final cell
	return @tmp_new_neighbors_atoms;
}
###################################
# Rules Cell Automata
sub Rules_Cell_Automata {
	my ($array_neighbors,$discretized_search_space) = @_;
	#
	my $delta = 0.2;
	#
	my @tmp_neighbors      = @{$array_neighbors};
	my $length_neighbors   = scalar (@{$array_neighbors});
	#
	my @search_space_atoms = @{$discretized_search_space};
	my $length_array_dis   = scalar (@{$discretized_search_space});
	###############################
	# my first cell in the space
	#
	if ( scalar (@tmp_new_neighbors_atoms) == 0 ) {
		my $int_coord_rand           = int rand($length_array_dis);
		my ($axis_x,$axis_y,$axis_z) = split '\s+', $search_space_atoms[$int_coord_rand];
		#
		my $element_1    = $tmp_neighbors[0];
		my $element_2    = $tmp_neighbors[1];
		my $radii_atom_1 = value_covalent_radii ($element_1);
		my $radii_atom_2 = value_covalent_radii ($element_2);
		my $total_radii  = $radii_atom_1 + $radii_atom_2;
		my $delta_radii  = $delta + $total_radii;
		#
		my $line_1 = "$element_1\t$axis_x\t$axis_y\t$axis_z";
		#
		my @array_tmp = ();
		foreach ( @search_space_atoms ) {
			my ($tmp_x,$tmp_y,$tmp_z) = split '\s+', $_;
			my $distance = Euclidean_distance ($axis_x,$axis_y,$axis_z,$tmp_x,$tmp_y,$tmp_z);
			if ( $distance > $total_radii && $distance <= $delta_radii ) {
				push (@array_tmp,"$element_2\t$tmp_x\t$tmp_y\t$tmp_z");
			}
		}
		my $tmp_length  = scalar (@array_tmp);
		my $tmp_int 	= int rand($tmp_length);
		my ($tmp_element,$tmp_x,$tmp_y,$tmp_z) = split '\s+', $array_tmp[$tmp_int];
		#
		my $line_2 = "$tmp_element\t$tmp_x\t$tmp_y\t$tmp_z";
		#
		@tmp_new_neighbors_atoms = ($line_1,$line_2);
		
	} else {
		# choose rand new neighbors
		my $length_new_neighbors              = scalar (@tmp_new_neighbors_atoms);
		#
		my $line;
		my $boolean = 0;
		# do...while loop execution			
		do {
			my $int_new_neighbors_rand            = int rand($length_new_neighbors);
			my ($element,$axis_x,$axis_y,$axis_z) = split '\s+', $tmp_new_neighbors_atoms[$int_new_neighbors_rand];
			# the last element of an array
			my $last_element = $tmp_neighbors[$#tmp_neighbors];
			#
			my $radii_atom_1 = value_covalent_radii ($element);
			my $radii_atom_2 = value_covalent_radii ($last_element);
			my $total_radii  = $radii_atom_1 + $radii_atom_2;
			my $delta_radii  = $delta + $total_radii;
			#
			my @array_tmp = ();
			foreach ( @search_space_atoms ) {
				my ($tmp_x,$tmp_y,$tmp_z) = split '\s+', $_;
				my $distance = Euclidean_distance ($axis_x,$axis_y,$axis_z,$tmp_x,$tmp_y,$tmp_z);
				if ( $distance > $total_radii && $distance <= $delta_radii ) {
					push (@array_tmp,"$last_element\t$tmp_x\t$tmp_y\t$tmp_z");
				}
			}
			#
			my $tmp_length  = scalar (@array_tmp);
			#
			my $tmp_int 	= int rand($tmp_length);
			my ($tmp_element,$tmp_x,$tmp_y,$tmp_z) = split '\s+', $array_tmp[$tmp_int];
			#
			my @arr = ();
			foreach (@tmp_new_neighbors_atoms) {
				push (@arr,$_);
				 print "$_\n";
			}
			$line = "$tmp_element\t$tmp_x\t$tmp_y\t$tmp_z";
			print "$line\n\n";
			push (@arr,$line);
			#
			$boolean = steric_impediment_atoms (\@arr);
			# print "$boolean \n";
			#
		} while ( $boolean > 0 );
		#	
		push (@tmp_new_neighbors_atoms,$line);
	}
}
###################################
# Center system
sub Center_All_Cell {
	my ($array_CS,$option) = @_;
	#
	my @Atoms   = ();
	my @coord_x = ();
	my @coord_y = ();	
	my @coord_z = ();
	#
	my @total_array = ();
	foreach my $line (@{$array_CS}) {
		if ( $option == 1 ) {
			my ($element,$axis_x,$axis_y,$axis_z) = split '\s+', $line;
			$Atoms[++$#Atoms]       = $element;
			$coord_x[++$#coord_x]   = $axis_x;
			$coord_y[++$#coord_y]   = $axis_y;
			$coord_z[++$#coord_z]   = $axis_z;
			#
		} else {
			my ($axis_x,$axis_y,$axis_z) = split '\s+', $line;
			$coord_x[++$#coord_x]   = $axis_x;
			$coord_y[++$#coord_y]   = $axis_y;
			$coord_z[++$#coord_z]   = $axis_z;
		
		} 	
	}
	#
	# for coords xyz molecules, moveby {x y z} (translate selected atoms to origin)
	my @array_center_mass = measure_center(\@coord_x,\@coord_y,\@coord_z);
	my @array_vecinvert   = vecinvert(\@array_center_mass);
	my @array_catersian   = vecadd (\@coord_x,\@coord_y,\@coord_z,\@array_vecinvert);
	if ( $option == 1 ) {		
		for ( my $i = 0 ; $i < scalar (@Atoms) ; $i = $i + 1 ){
			push (@total_array,"$Atoms[$i]\t$array_catersian[0][$i]\t$array_catersian[1][$i]\t$array_catersian[2][$i]");
		}
	} else {
		for ( my $i = 0 ; $i < scalar (@coord_x) ; $i = $i + 1 ){
			push (@total_array,"$array_catersian[0][$i]\t$array_catersian[1][$i]\t$array_catersian[2][$i]");
		}
	}	
	#
	return (@total_array);
}


# acuerdate para que los fragemtos esten dentro de 
# la caja lo puedes hacer con min and max, por lo tanto
# con eso pyedes ver todo
###################################
# MAIN
#
my $tiempo_inicial  = new Benchmark; #funcion para el tiempo de ejecucion del programa
#
my $a_1   = "chemical_formula = C 05 Pb 03";
#my $a_1   = "chemical_formula = O 01 Pb 03 Cu 05 Ru 01";
my @tmp   = ();
my @Atoms = ();
my $Num_of_atoms = 0;

@tmp    = split (/\s+/,$a_1);
# Identify empty string
if (!defined($tmp[2])) {
	print "WARNING chemical formula empty\n";
	$Num_of_atoms = 0;
} else {
	@Atoms = looks_like_number (\@tmp);
	$Num_of_atoms = scalar (@Atoms);
}

# Parametros
my $side_box        = automatic_box_length (\@Atoms);
my $length_side_box = ($side_box * 2);
print "Length Side Box = $length_side_box A\n";
#
# Fix Value 0.4
my $cell_size_w     = 0.4;
#
my @discretized_search_space = Construct_Discrete_Search_Space_Cube ($length_side_box,$cell_size_w);
#
print "Espacio listo\n";
my $ncpus = 80;
my $pm    = new Parallel::ForkManager($ncpus);
#
my $file_tmp = "coords.xyz";
open (FILE, ">$file_tmp") or die "Unable to open XYZ file: $file_tmp";
for (my $x=0; $x < 1 ; $x++) {
	$pm->start($x) and next;
	# All children process havee their own random.			
	srand();
	my @shuffled_atoms           = fisher_yates_shuffle (\@Atoms);
	my @array_CS                 = Construct_Solutions ($Num_of_atoms,\@shuffled_atoms,\@discretized_search_space);
	my @final_construct_molecule = Center_All_Cell (\@array_CS,1);
	#
	print FILE "$Num_of_atoms\n0.000000 Kcal/mol 0.000000 eV -1968.764159 H\n";
	for ( my $i = 0 ; $i < scalar (@final_construct_molecule) ; $i = $i + 1 ) {
		my ($element,$axis_x,$axis_y,$axis_z) = split '\s+', $final_construct_molecule[$i];
		#
		my $axis_x_oxs  = sprintf '%.4f', $axis_x;
		my $axis_y_oxs  = sprintf '%.4f', $axis_y;
		my $axis_z_oxs  = sprintf '%.4f', $axis_z; 
		print FILE "$element  $axis_x_oxs  $axis_y_oxs  $axis_z_oxs\n";
#		push (@total_array,"$final_construct_molecule[0][$i]\t$final_construct_molecule[1][$i]\t$final_construct_molecule[2][$i]\t$final_construct_molecule[3][$i]");
	}
	$pm->finish;	
}
close (FILE);
# Paralel
$pm->wait_all_children;
#
my $tiempo_final  = new Benchmark;
my $tiempo_total  = timediff($tiempo_final, $tiempo_inicial);
print "\n\tTiempo de ejecucion: ",timestr($tiempo_total),"\n";
print "\n";
#
exit 0;
