#!/usr/bin/env perl
#!/usr/bin/perl -s


use strict;
use warnings; no warnings 'uninitialized';
use Data::Dump qw(dump ddx);


my %Atomic_number = ( '89'  => 'Ac', '13'  => 'Al', '95'  => 'Am', '51'  => 'Sb',	
	                  '18'  => 'Ar', '33'  => 'As', '85'  => 'At', '16'  => 'S',  
					  '56'  => 'Ba', '4'   => 'Be', '97'  => 'Bk', '83'  => 'Bi',	
                      '107' => 'Bh', '5'   => 'B', 	'35'  => 'Br', '48'  => 'Cd',	
	                  '20'  => 'Ca', '98'  => 'Cf',	'6'   => 'C',  '58'  => 'Ce',	
	                  '55'  => 'Cs', '17'  => 'Cl',	'27'  => 'Co', '29'  => 'Cu',	
	                  '24'  => 'Cr', '96'  => 'Cm', '110' => 'Ds', '66'  => 'Dy',
	                  '105' => 'Db', '99'  => 'Es', '68'  => 'Er', '21'  => 'Sc',	
	                  '50'  => 'Sn', '38'  => 'Sr', '63'  => 'Eu', '100' => 'Fm',	
	                  '9'   => 'F',  '15'  => 'P',  '87'  => 'Fr', '64'  => 'Gd',	
	                  '31'  => 'Ga', '32'  => 'Ge', '72'  => 'Hf', '108' => 'Hs',	
                      '2'   => 'He', '1'   => 'H',  '26'  => 'Fe', '67'  => 'Ho',	
					  '49'  => 'In', '53'  => 'I',  '77'  => 'Ir', '70'  => 'Yb',
					  '39'  => 'Y',  '36'  => 'Kr', '57'  => 'La', '103' => 'Lr',	
					  '3'   => 'Li', '71'  => 'Lu', '12'  => 'Mg', '25'  => 'Mn',	
                      '109' => 'Mt', '101' => 'Md', '80'  => 'Hg', '42'  => 'Mo',	
					  '60'  => 'Nd', '10'  => 'Ne', '93'  => 'Np', '41'  => 'Nb',	
					  '28'  => 'Ni', '7'   => 'N',  '102' => 'No', '79'  => 'Au',	
					  '76'  => 'Os', '8'   => 'O', 	'46'  => 'Pd', '47'  => 'Ag',	
					  '78'  => 'Pt', '82'  => 'Pb',	'94'  => 'Pu', '84'  => 'Po',	
					  '19'  => 'K',  '59'  => 'Pr', '61'  => 'Pm', '91'  => 'Pa',	
					  '88'  => 'Ra', '86'  => 'Rn', '75'  => 'Re', '45'  => 'Rh',	
					  '37'  => 'Rb', '44'  => 'Ru', '104' => 'Rf', '62'  => 'Sm',
					  '106' => 'Sg', '34'  => 'Se', '14'  => 'Si', '11'  => 'Na',
					  '81'  => 'Tl', '73'  => 'Ta', '43'  => 'Tc', '52'  => 'Te',	
					  '65'  => 'Tb', '22'  => 'Ti', '90'  => 'Th', '69'  => 'Tm',	
					  '112' => 'Uub','116' => 'Uuh','111' => 'Uuu','118' => 'Uuo',	
					  '115' => 'Uup','114' => 'Uuq','117' => 'Uus','113' => 'Uut',
					  '92'  => 'U',  '23'  => 'V',  '74'  => 'W',  '54'  => 'Xe',
                      '30'  => 'Zn', '40'  => 'Zr', 'X' => '0.5' );

my $Input_noExt;

# compute the center of mass
sub measure_center {
	my ($coord_x,$coord_y,$coord_z) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array  = ();
	my $weight = 1;
	# variable sum
	my $sum_weight = 0;
	my $sum_x = 0;
	my $sum_y = 0;
	my $sum_z = 0;
	for ( my $j = 0 ; $j < $num_data ; $j = $j + 1 ){
		$sum_weight+= $weight;
		$sum_x+= $weight * @$coord_x[$j];
		$sum_y+= $weight * @$coord_y[$j];
		$sum_z+= $weight * @$coord_z[$j];		
	}
	my $com_x = $sum_x / $sum_weight;
	my $com_y = $sum_y / $sum_weight;
	my $com_z = $sum_z / $sum_weight;
	# array
	@array = ($com_x,$com_y,$com_z);
	# return array	
	return @array;
}
###################################
# Returns the additive inverse of v(-v)
sub vecinvert {
	my ($center_mass) = @_;
	my @array         = ();
	foreach my $i (@$center_mass) {
		my $invert        = $i * -1;
		$array[++$#array] = $invert; 
	}	
	# return array	
	return @array;
}
###################################
# Returns the vector sum of all the terms.
sub vecadd {
	my ($coord_x,$coord_y,$coord_z,$vecinvert_cm ) = @_;
	my $num_data = scalar (@{$coord_x});
	my @array   = ();
	my $sum_coord_x;
	my $sum_coord_y;
	my $sum_coord_z;
	# array 
	my @array_x = ();
	my @array_y = ();
	my @array_z = ();
	for ( my $i = 0 ; $i < $num_data ; $i = $i + 1 ){	
		$sum_coord_x = @$coord_x[$i]+@$vecinvert_cm[0] ; 
		$sum_coord_y = @$coord_y[$i]+@$vecinvert_cm[1] ;
		$sum_coord_z = @$coord_z[$i]+@$vecinvert_cm[2] ;
		# save array
		$array_x[++$#array_x] = $sum_coord_x;
		$array_y[++$#array_y] = $sum_coord_y;
		$array_z[++$#array_z] = $sum_coord_z;
	}
	@array = ( [@array_x], 
              [@array_y], 
              [@array_z] ); 
	# return array	
	return @array;
}
sub CenterMolecule{
	my ($input)=@_;
	my @problematic_molecule=@{$input};
	my @coord_x=@{$problematic_molecule[1]};
	my @total_array = ();
	
	my @array_center_mass = measure_center(\@{$problematic_molecule[1]},\@{$problematic_molecule[2]},\@{$problematic_molecule[3]});
	my @array_vecinvert   = vecinvert(\@array_center_mass);
	my @array_catersian   = vecadd (\@{$problematic_molecule[1]},\@{$problematic_molecule[2]},\@{$problematic_molecule[3]},\@array_vecinvert);
	@total_array=(${problematic_molecule[0]},
					$array_catersian[0],
					$array_catersian[1],
					$array_catersian[2]);
	return (@total_array);	
}
sub ReadXYZFile{
	my ($file)=@_;

	my @at = ();
	my @cx = ();
	my @cy = ();
	my @cz = ();

	my $flagCycle=1;
	my $natom;
	my $title;
	my $aux=0;
	my %isomers;

	if(open (FILE, "$file")){
		foreach my $line(<FILE>){
			chomp ($line);
			if($flagCycle==1){
				#NATOM
				$natom=$line;
				$flagCycle=2;
				#print "-> $line";
			}elsif($flagCycle==2){
				#TITLE
				$title=$line;
				$flagCycle=3;
			}elsif($natom eq $line){
				#End of a cycle
				my @tmpCoords=([@at],
								[@cx],
								[@cy],
								[@cz]);
				#Center
				my @centered=CenterMolecule(\@tmpCoords);
				$isomers{$aux}=[@centered,$title,$natom];
				#print "TITLE: $title\n";
				#dump(@centered);
				$aux++;
				@at = ();
				@cx = ();
				@cy = ();
				@cz = ();

				$flagCycle=2;
			}else{
				#COORDS
				my ($a, $x, $y, $z)=split(" ", $line);
				push @at, $a;
				push @cx, $x;
				push @cy, $y;
				push @cz, $z;
			}
		}
		my @tmpCoords=([@at],
						[@cx],
						[@cy],
						[@cz]);
		#Center
		my @centered=CenterMolecule(\@tmpCoords);
		$isomers{$aux}=[@centered,$title,$natom];
		#dump(%isomers);		
		#print "\n-> $isomers{0}\n";
	}else{
		print "Warning: couldn't read $file\n";
	}	
	return %isomers;
}
sub CalculateDistance{
	my ($x, $y, $z)=@_;
	my $dist=($x**2)+($y**2)+($z**2);
	#print "$x\t$y\t$z\t$dist\n";
	my $root=sqrt($dist);
	return $root;
}
sub CalculateSimEquation{
	my (%centeredCoords)=@_;
	#my %atomTypeWeigth=("X"  => 1, "cont" =>1);
	#print "Loop Start\n";
	my $keysCount=keys %centeredCoords;
	#print "Llaves : $keysCount\n";
	my %Sumas;
	for (my $i = 0; $i < $keysCount; $i++) {
		#print "-->$i\n";
		my @data=$centeredCoords{$i};
		#dump @data;
		my @at=@{$data[0][0]};
		my @cx=@{$data[0][1]};
		my @cy=@{$data[0][2]};
		my @cz=@{$data[0][3]};

		#dump @at;
		my $suma=0;
		for (my $j = 0; $j <= $#at; $j++) {
			#print" $at[$j] $cx[$j] $cy[$j] $cz[$j]\n";
			#if(exists $atomTypeWeigth{$at[$j]}){
				#dump %atomTypeWeigth;
			#}else{
			#	$atomTypeWeigth{$at[$j]}=$atomTypeWeigth{"cont"}+1;
			#	$atomTypeWeigth{"cont"}++;
			#}

			my $dist=CalculateDistance($cx[$j],$cy[$j],$cz[$j]);
		#	print "$dist\n";
			my $sumando=$Atomic_number{$at[$j]}*($dist**2);
			$suma=$suma+$sumando;

		}
		#push @Sumas, $suma;
		$Sumas{$i}=$suma;
		$suma=0;
		
		#determinar tipo de atomo y asignar peso, siempre el mismo.
		# M*dt**2
		# M es peso y dt es la distancia euclidiana
		#print "change\n";
		#print "We are in Disst\n";
		#last;
	}
	#dump %Sumas;
	#last
	return %Sumas;
}
sub NewXYZ{
	my ($coords, $sums)=@_;
	my %Coords=%{$coords};
	my %Sums=%{$sums};
	my $deltaMagico=0.5;
	my $llaves= keys %Sums;
	foreach my $i (keys %Sums){
		foreach my $j (keys %Sums){
			if((abs($Sums{$i}-$Sums{$j})) < $deltaMagico){
				if($i==$j){
					#print "Same - ERROR\n";
				}elsif($i<$j){
					#print "i es mejor\n";
					delete $Sums{$j};
				}else{
					#print "j es mejor\n";
					delete $Sums{$i};
				}
			}
		}
	}
	open (XYZ, ">$Input_noExt\_unique.xyz");
	my @keys=keys %Sums;
	my @sorted= sort {$a <=> $b} @keys;
	#dump @sorted; 
	#dump %Coords;
	foreach my $element(@sorted){
		#print XYZ "$Coords[$element][5]\n$Coords[$element][4]\n";
		my @tmp=$Coords{$element};
		#dump @tmp;
		print XYZ "$tmp[0][5]\n$tmp[0][4]\n";
		for (my $j = 0; $j <= $#{$tmp[0][0]}; $j++) {
			print XYZ "$tmp[0][0][$j]\t$tmp[0][1][$j]\t$tmp[0][2][$j]\t$tmp[0][3][$j]\n"
		}
		#last;
	}

}
($Input_noExt = $ARGV[0]) =~ s/\.[^.]+$//;
my %centeredCoords=ReadXYZFile($ARGV[0]);
#dump(%centeredCoords);
my %has1=CalculateSimEquation(%centeredCoords);
NewXYZ(\%centeredCoords,\%has1);

