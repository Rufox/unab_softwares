#!/usr/bin/env perl

use strict;
use warnings;

my @tmp_new_neighbors_atoms = ();
#
my $other_element = 0.8;
my %Atomic_radii = ( 'H'  => '0.4', 'He' => '0.3', 'Li' => '1.3', 'Be' => '0.9', 
                     'B'  => '0.8', 'C'  => '0.8', 'N'  => '0.8', 'O'  => '0.7', 
					 'F'  => '0.7', 'Ne' => '0.7', 'Na' => '1.5', 'Mg' => '1.3', 
					 'Al' => '1.2', 'Si' => '1.1', 'P'  => '1.1', 'S'  => '1.0', 
					 'Cl' => '1.0', 'Ar' => '1.0', 'K'  => '2.0', 'Ca' => '1.7', 
					 'Sc' => '1.4', 'Ti' => '1.4', 'V'  => '1.3', 'Cr' => '1.3', 
					 'Mn' => '1.4', 'Fe' => '1.3', 'Co' => '1.3', 'Ni' => '1.2', 
					 'Cu' => '1.4', 'Zn' => '1.3', 'Ga' => '1.3', 'Ge' => '1.2', 
					 'As' => '1.2', 'Se' => '1.2', 'Br' => '1.1', 'Kr' => '1.1', 
					 'Rb' => '2.1', 'Sr' => '1.9', 'Y'  => '1.6', 'Zr' => '1.5', 
					 'Nb' => '1.4', 'Au' => '1.4' );


sub value_covalent_radii {
	my ($element) = @_;
	my $radii_val = 0;
	if ( exists $Atomic_radii{$element} ) {
		$radii_val = $Atomic_radii{$element};
	} else {
		$radii_val = $other_element ;
	}	
	return $radii_val
}

sub fisher_yates_shuffle {
	my $array = shift;
	my $i = @$array;
	while ( --$i ) {
		my $j = int rand( $i+1 );
		@$array[$i,$j] = @$array[$j,$i];
	}
	return @$array; 
}

sub read_file {
	# filename
	my ($input_file) = @_;
	my @array = ();
	# open file
	open(FILE, "<", $input_file ) || die "Can't open $input_file: $!";
	while (my $row = <FILE>) {
		chomp($row);
		push (@array,$row);
	}
	close (FILE);
	# return array	
	return @array;
}




#
my $delta           = 0.5;
#
my @tmp             = read_file ("cb7.cart");
#
my $number_elements = scalar (@tmp);
#
for (my $j=0; $j < 100 ; $j++) {
	# Genes o atomos que se toman para realizar la mutacion.
	my $int_coord_rand  = int rand($number_elements);
	#
	# Recombinacion de los genes o atomos
	my @recombinacion   = fisher_yates_shuffle (\@tmp);
	#
	#
	print "16\n\n";
	for (my $i=0; $i < $number_elements ; $i++) {
		my ($element,$coord_x,$coord_y,$coord_z) = split (/\s+/,$recombinacion[$i]);
		# Todos los valores menor e igual van a hacer los genes o atomos que se van a mutar
		if ( ($i <= $int_coord_rand ) && ($j >= 1 )  )  {
			#
			my $radii_atom  = value_covalent_radii ($element);
			my $total_radii = ($radii_atom + $radii_atom);
			my $delta_radii = $delta + $total_radii ;
			#
			my $num         = $radii_atom + rand($delta_radii - $radii_atom);
			#
			my $sum_x = $num + $coord_x ;
			my $sum_y = $num + $coord_y ;
			my $sum_z = $num + $coord_z ;		
			#
			print "$element $sum_x  $sum_y  $sum_z\n";
			#
		} else {
			print "$element $coord_x $coord_y $coord_z \n";	
		}
	}
}
#
exit 0;
