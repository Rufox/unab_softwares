#!/usr/bin/env perl
#!/usr/bin/perl -s


#Este programa tiene como finalidad empezar el proceso de ensamble para un cluster en eepscifico
#en resumen hara:
#leera un archivo .txt con todas las variables a considerar
#-evaluara las pposibles combinaciones a hacer para u clsuter en especifico
#-recorrera los repositorios buscando los fragmentos a utilizar
#-evalaura en base al valor del potenical quimico que fragmento es el aceptor y donador de electrones.
#-Correra el Rubik
#-Creacion de archivo COm.
#-Mantendra un history de las combinaciones y com creados.

# Tarzar English
# Atenas (Trapen)

 # Notas de Version
 # Navigates through repository to get information.
 # Read the chemical potential from the log file in repository
 # Evaluate all possible assembly to build an specific cluster.
 # Determine wich fragment is nucleophilic or electrophilic fragment.
 # Call Rubik to assemble
 # Create .com files to gaussian.
 # Create txtx file twith all the combination done.
 # Moves fiels to folders.
 # Reads a configuration file with all the information.

use Benchmark;
use Cwd qw(abs_path);
use File::Basename;
#use strict;
#use warnings;

my $ConfifFile=$ARGV[0];
my $Rubik="Rubik_v10.pl";
my $COMFILE="COMfileCreator_v2.pl";
my $TAFFMaker="Frag_file_maker.pl";
my $OPTION=2;
#Variables_Ensamble
my $NumGeom;
my $Input;
my $Approx;
my $xyzSize;
my @IgnoreFragments=();
my $Box=-1;
my $Sort=-1;
#Variables_Com
my $GaussianName;
my $Command;
my $Nproc=4;
my $RAM=4;
my $Charge;
my $Multi;
my $NumCOMs;
#Variables_<Generaless con repositorio
my $repositoryroute;
my $type;
# Variables generales sin repositorio.
my $Compute="-f";
my $catin1="";
my $catin2="";
my $anin1="";
my $anin2="";
my $in1;
my $in2;
# Variable Globales
my $directory_error;


sub ltrim { my $s = shift; $s =~ s/^\s+//;       return $s };
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
sub ConfigFileReader{
	open(CONFIG,"$ConfifFile") or die "Configuration file doesn't exist\n";
	my @lines=<CONFIG>;
	close(CONFIG);

	my $aux=0;
	my $root=dirname(abs_path($0));
	$Rubik="$root/".$Rubik;
	$TAFFMaker="$root/".$TAFFMaker;
	$COMFILE="$root/".$COMFILE;
	foreach my $line(@lines){
		chomp($line);
		my $letter = substr($line, 0, 1);
		my ($key, $value)=split("=",$line);
		# Check repository 
		#1
		# the char # marks a commentary in Miconf.txt
		if($letter ne "#" && $line ne ""){
			if ($key=~/repository/i) {
				if($value=~/no/i){
					$OPTION=1; #1 is no repository
				}elsif($value=~/yes/i  || $value=~/si/i){
					$OPTION=0; #0 means repositroy
				}else{
					print "Error, repository not specificied\n";
					exit(1);
				}
				$aux++;
			
			}if($OPTION==0){  #Params for repository
				#2i
				if($key=~/filetype/i){
					$type=$value;
					$aux++;
				}
				#3i
				if($key=~/reproute/i){
					$repositoryroute=$value;
					$aux++;
				}
				if($key=~/input/i){
					$Input=$value;
					$aux++;
				}
			}else{
				#2i
				if($key=~/neutralinp1/i){
					$in1=trim($value);
					$aux++;
				}
				#3i
				if($key=~/neutralinp2/i){
					$in2=trim($value);
					$aux++;
				}
				if($key=~/catin1/i){
					$catin1=trim($value);
				}
				if($key=~/anin1/i){
					$anin1=trim($value);
				}
				if($key=~/catin2/i){
					$catin2=trim($value);
				}
				if($key=~/anin2/i){
					$anin2=trim($value);
				}
			}
			#2
			if($key=~/numberofassemblies/i){
				$NumGeom=$value;
				$aux++;

			}
			if($key=~/approximation/i){
				if ($value=~/koopmans/i) {
					$Approx="Koopmans";
				} elsif ($value=~/finitedifferences/i) {
					$Approx="FiniteDifferences";
				} else {
					print "Error in Approximation\n";
					exit(1);
				}
				$aux++;
			}
			#5
			if ($key=~/numberoffinalstructures/i) {
				$xyzSize=$value;
				$aux++;
			}
			#6
			if($key=~/output/i){
				$GaussianName=$value;
				$aux++;
			}
			if(($key=~/command/i) || ($key=~/charge/i) || ($key=~/multiplicity/i)){$aux++}
			if ($key=~/numbercoms/i){
				$NumCOMs=$value;
				$aux++
			}	
			if($key=~/ignorefragments/i){
				@IgnoreFragments=split(" ",$value);
			}
			if($key=~/boxsize/i){
				#@IgnoreFragments=split(" ",$value);
				$Box=$value;
			}
			if($key=~/sort/i){
				#@IgnoreFragments=split(" ",$value);
				$Sort=$value;
			}
	}
	}
	if($OPTION==0 && $aux!=12){
		print "Incorrect number of arguments in $ConfifFile - $aux;\n";
		exit(1);
	}elsif($OPTION==1 && $Approx=~/koopmans/i && $aux!=11){
		print "Incorrect number of arguments in $ConfifFile +\n";
		exit(1);
	}elsif($OPTION==1 && $Approx=~/finitedifferences/i && $aux<11){	
		print "Incorrect number of arguments in $ConfifFile *\n";
		exit(1);
	}else{	
		print "Program Started\n\n";
	}
}
sub ValidateFilesGiven{
	my ($app,@files)=@_;
	my ($name1,$ext1)=ValidateFile($files[0]);
	my ($name2,$ext2)=ValidateFile($files[1]);
	my $fchFlag=0;
	if($ext1 eq $ext2){
		if($ext1 eq ".fch"){
			$fchFlag=1;
		}elsif($ext1 ne ".frag" && $ext1 ne ".bas"){
			print "Format of input files not supported ($ext1)\n";
			exit(1);
		}
	}else{
		print "Program cant open 2 files with differents extensions\n";
		exit(1);
	}
	if($app eq "FiniteDifferences" && $fchFlag==1){
		ValidateFile($files[2]);
		ValidateFile($files[3]);
		ValidateFile($files[4]);
		ValidateFile($files[5]);	
	}
	return $fchFlag;
}
sub ValidateFile{
	my ($fullspec)=@_;	
	die ("Can't open $fullspec\n") if !(-e $fullspec);
	my($file, $dir, $ext) = fileparse($fullspec, qr/\.[^.]*/);
	#print "Directory: " . $dir . "\n";
	#print "File:      " . $file . "\n";
	#print "Suffix:    " . $ext . "\n\n";
	return ($file, $ext);
}
sub ReadLogFile{
	my ( $type, $logname)=@_;
	my $ChemPot;
	my @lines=();
	if($type==1){
		open(LOG,$logname);
		@lines=<LOG>;
		close(LOG);
	}else{
		@lines=@{$logname};
	}
	foreach my $line(@lines){
		chomp($line);
		if($line=~/Chemical potential/){
			$ChemPot=ltrim((split(":",$line))[1]);
			last;
		}
	}
	return $ChemPot;
}
sub GetAllChemicalPotentials{
	my($atom1,$number1)=@_;
	my @ChemicalsPotentials=();
	#Lo siguiente sirve para saber cuantas estructuras existen para un cluter en especifo
	my $directory_name = "$repositoryroute/$atom1/$atom1$number1/$Approx/"|| '.';
	if ((opendir (my $dh, $directory_name) )!=0){
	  my $file_count = scalar grep { -d "$directory_name/$_" && !/^\.{1,2}$/ } readdir $dh;
	  closedir $dh or die "Can't closedir: $!";
	  #print "numero de files es $file_count\n";
	  #Luego se recorren todas las estructuras buscando el arhicov .log con la informacion del potencial quimico.
	  for (my $i = 0; $i < $file_count; $i++) {
  		my $directory_tmp="$directory_name/$i";
  		opendir my $ditmp, $directory_tmp or die "Can't opendir '$directory_tmp'";
	  	my @Logfiles=grep (/\.log$/,readdir $ditmp);
  		push @ChemicalsPotentials,ReadLogFile(1,"$directory_tmp/$Logfiles[0]");
  		closedir $ditmp;
  	}
  }else{
    print "Issues in $directory_name . Maybe the directory doesn't exists?\n";
    $directory_error=0;
  }

	return @ChemicalsPotentials;
}
sub GetFragments{
	my ($directory,$symbol)=@_;
	my @files=();
	opendir my $ditmp, $directory or die "Can't opendir '$directory'";
	if($symbol eq "+"){
		@files=grep (/^f\+/ && /\.frag$/ ,readdir $ditmp);
	}else{
		@files=grep (/^f-/ && /\.frag$/,readdir $ditmp);
	}
	return $files[0];
}
sub CalculatePossibleAssembly{
	$Input =~ /([0-9]+)/; 
	my $number =$1;
	my @args=();
	my ($atom)=split/[0-9]/,$Input;

	open(RESUME,">Resume.txt");
	#Change this (2) to 1, to add the (Atom +N-1) assembly
	for (my $i = 1; $i <= (($number-2)/2)+1; $i++) {
		my $secondNumber=$number-$i;
		#Ignore the following fragments for assembly
		if ( grep( /^$i$/, @IgnoreFragments ) ) {
			print "Discarting assembly $i + $secondNumber by user's petition\n";
		}else{
	   		$directory_error=1;   #no existe error de directorio
			my @Chem1=GetAllChemicalPotentials($atom,$i);
			my @Chem2=GetAllChemicalPotentials($atom,$secondNumber);
			#		print "$i = @Chem1\n";
#		print "$secondNumber = @Chem2\n";
	   	if($directory_error==1){  #Ambos directorios existen en repositorio
			OUT: for (my $Frag1 = 0; $Frag1 <= $#Chem1; $Frag1++) {
					for (my $Frag2 = 0; $Frag2 <= $#Chem2; $Frag2++) {
						my $directory_name1 = "$repositoryroute/$atom/$atom$i/$Approx/$Frag1";		#f+
						my $directory_name2 = "$repositoryroute/$atom/$atom$secondNumber/$Approx/$Frag2";		#f-
						if($Chem1[$Frag1]<$Chem2[$Frag2]){
							#print "f+ f-  $Chem1[$Frag1]  <  $Chem2[$Frag2]\n";
							my $fPlus=GetFragments($directory_name1,"+");
							my $fMinus=GetFragments($directory_name2,"-");
							RubikConfigFileCreator("$directory_name1/$fPlus","$directory_name2/$fMinus");
				#			@args=($Rubik,"-$type","$directory_name1/$fPlus","$directory_name2/$fMinus",$NumGeom, $xyzSize);
							print RESUME "f+ = $fPlus\tf- = $fMinus\n";
							print RESUME "$Chem1[$Frag1]  <  $Chem2[$Frag2]\n\n"
							#last OUT;
						}else{
							#print "f- f+  $Chem1[$Frag1]  >  $Chem2[$Frag2]\n";
							my $fMinus=GetFragments($directory_name1,"-");
							my $fPlus=GetFragments($directory_name2,"+");
							RubikConfigFileCreator("$directory_name2/$fPlus","$directory_name1/$fMinus");
							#@args=($Rubik,"-$type","$directory_name2/$fPlus","$directory_name1/$fMinus",$NumGeom, $xyzSize);
							print RESUME "f+ = $fPlus\tf- = $fMinus\n";
							print RESUME "$Chem1[$Frag1]  >  $Chem2[$Frag2]\n\n"
							
						}
	#				print "Linea:  @args\n";
					#system($^X,@args);
					`perl $Rubik RuConfTMP.in`;
					unlink "RuConfTMP.in";
					}
				#last;
				}
	   		}
	   	}
	}
	close(RESUME);
}
###########################
######COMS"
###########################
sub MvFilesCreated{
	my @files_xyz =glob "*.xyz";
	my @files_log =glob "*.txt";
	my @files_com =glob "*.com";

	print "Verficando el directorio existe\n";
	if(-d "XYZ"){
		print "Se encontró el directorio XYZ\n";
	}else{
		mkdir "XYZ";
	}
	foreach my $xyz (@files_xyz) {
		rename $xyz, "XYZ/$xyz";
	}

	if(-d "TXT"){
		print "Se encontró el directorio TXT\n";
	}else{
		mkdir "TXT";
	}
	foreach my $log (@files_log) {
		rename $log, "TXT/$log";
	}

	if(-d "COMS"){
		print "Se encontró el directorio COMS\n";
	}else{
		mkdir "COMS";
	}
	foreach my $com (@files_com) {
		rename $com, "COMS/$com";
	}
}
sub GaussainFilesGenerator{
	
	#unlink glob "*.com"; #deletes all the .com files in route.
	my @files=glob "*.xyz";
	my @args=();
	my $aux=0;
	open(SYM,">Simbologia.txt");
	foreach my $xyz (@files) {
		print SYM "$GaussianName-$aux  ====> $xyz\n";
		@args=($COMFILE,"-q","$NumCOMs","$ConfifFile","$xyz","$GaussianName-$aux");
		system($^X,@args);
		$aux++;
	}
	close(SYM);
}
sub RubikConfigFileCreator{
	my ($file1, $file2)=@_;
	open(RTMP, ">RuConfTMP.in");
	print RTMP "NumberOfAssemblies = $NumGeom\n";
	print RTMP "NumberOfFinalStructures =$xyzSize\n";

	print RTMP "Type=-taff\n";
	print RTMP "Fragment1=$file1\n";
	print RTMP "Fragment2=$file2\n";
	if ($Box!=-1){
		print RTMP "BoxSizeFactor=$Box\n";
	}
	if ($Sort!=-1) {
		print RTMP "Sort =$Sort\n";
	}
	close (RTMP);
}
sub ManualFchCase{
	my ($In1_noExt, $In2_noExt)=@_;
	my ($In1_noExt)=ValidateFile($in1);
	my ($In2_noExt)=ValidateFile($in2);
	my $FolderPre, $MiniName;
	my $Chem1, $Chem2;
	my @log1=(), @log2=();
	print "Lanzando TAFF\n";
	if($Approx=~/koopmans/i){
		$FolderPre="Koopmans";
		$MiniName="koop";
		@log1=`TAFF -k $Compute $in1`;
		@log2=`TAFF -k $Compute $in2`;
		#print "#####################\n@magic\n";
		$Chem1=ReadLogFile(2, \@log1);
		$Chem2=ReadLogFile(2, \@log2);
	}else{
		$FolderPre="FiniteDifferences";
		$MiniName="_dif";
		@log1=`TAFF -fd $Compute $in1 $catin1 $anin1`;
		@log2=`TAFF -fd $Compute $in2 $catin2 $anin2`;
		#print "#####################\n@magic\n";
		$Chem1=ReadLogFile(2, \@log1);
		$Chem2=ReadLogFile(2, \@log2);
	}
	#TAFF MAKER .tf FILE
	print "Formateando\n";
	if($Chem1<$Chem2){
		system($^X, "$TAFFMaker", "$FolderPre\_txt/f+$MiniName-$In1_noExt-integral.txt", "$FolderPre\_cube/f+$MiniName-$In1_noExt.cube");
		system($^X, "$TAFFMaker", "$FolderPre\_txt/f-$MiniName-$In2_noExt-integral.txt", "$FolderPre\_cube/f-$MiniName-$In2_noExt.cube");
		print "\n\t\tf+\t\t\t\tf-\n\t$in1\t\t\t\t$in2\nChemPot: $Chem1\t\t<\t\t$Chem2\nEnsamblando...\n\n";
		#system($^X, "$Rubik", "-taff", "f+$MiniName-$In1_noExt.tf", "f-$MiniName-$In2_noExt.tf", "$NumGeom", "$xyzSize");
		RubikConfigFileCreator("f+$MiniName-$In1_noExt.frag","f-$MiniName-$In2_noExt.frag");
		#`perl $Rubik -taff f+$MiniName-$In1_noExt.tf f-$MiniName-$In2_noExt.tf $NumGeom $xyzSize`;
	}else{
		system($^X, "$TAFFMaker", "$FolderPre\_txt/f+$MiniName-$In2_noExt-integral.txt", "$FolderPre\_cube/f+$MiniName-$In2_noExt.cube");
		system($^X, "$TAFFMaker", "$FolderPre\_txt/f-$MiniName-$In1_noExt-integral.txt", "$FolderPre\_cube/f-$MiniName-$In1_noExt.cube");
		print "\n\tf+\t\t\t\tf-\n$in2\t\t\t\t$in1\nChemPot: $Chem2\t\t<\t\t$Chem1\nEnsamblando...\n\n";
		RubikConfigFileCreator("f+$MiniName-$In2_noExt.frag", "f-$MiniName-$In1_noExt.frag");
		#`perl $Rubik -taff f+$MiniName-$In2_noExt.tf f-$MiniName-$In1_noExt.tf $NumGeom $xyzSize`;
	}
	`perl $Rubik RuConfTMP.in`;
	#exit(1);
	``;
	`rm -r $FolderPre*`
}
sub ManualFragCase{
	RubikConfigFileCreator($in1,$in2);
	`perl $Rubik RuConfTMP.in`;
}
#Time reader
my $tiempo_inicial = new Benchmark; #funcion para el tiempo de ejecucion del programa
$datestringStart = localtime();
#
ConfigFileReader();
if($OPTION==0){  #there is repository
	print "Lanzando Ensambles\n";
	CalculatePossibleAssembly();
}else{  #no repository
	my $CaseOption;
	if($Approx eq "Koopmans"){
		$CaseOption=ValidateFilesGiven($Approx,($in1,$in2));
	}else{
		$CaseOption=ValidateFilesGiven($Approx,($in1,$in2,$catin1,$catin2,$anin1,$anin2));
	}
	# Format Input file distinguer
	if($CaseOption==1){
		ManualFchCase();	
	}else{
		ManualFragCase();
	}
	
	
}
#Time until assembly done
unlink glob "*.cart";
unlink glob "*noSort.xyz";
print "Construyendo .com\n";
GaussainFilesGenerator();
MvFilesCreated();
# Time with .com files done.		

my $tiempo_final = new Benchmark;
$datestringEnd = localtime();
#my $tiempo_secondBloq = timediff($tiempo_final, $tiempo_medio);
my $tiempo_allBloq = timediff($tiempo_final, $tiempo_inicial);
#print "\n\tTiempo de Ensamble: ",timestr($tiempo_firstBloq),"\n";
#print "\n\tTiempo de Creacion de Archivos: ",timestr($tiempo_secondBloq),"\n";
print "\n\tTiempo de Total: ",timestr($tiempo_allBloq),"\n";
print "\n";
	
open(RESUME,">>Resume.txt");
print RESUME "\n\tTiempo de Total: ",timestr($tiempo_allBloq),"\n";
close(RESUME);

print "All Good\n";
